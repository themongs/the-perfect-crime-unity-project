# The Perfect Crime - Unity Project

This is the Unity3D project folder for the group project for the team "The Fighting Mongooses", in RMIT BITS SP2 2016. It may not contain all binary assets needed to run the game, they will be listed at the bottom of this README file.

## Cloning

1. Open up the Unity Project in Bitbucket.org, and copy the URL on the top right of the Overview page, for example:

`https://userid@bitbucket.org/themongs/the-perfect-crime-unity-project.git`

2a. Open up SourceTree, click "Clone/New" and paste the above URL in the "Source Path / URL" box. It should think for a moment, then "Repository Type" should say "This is a Git repository". If it errors, ask for help on the email thread.

You can change the destination path to something neater, like `C:\users\default\documents\PerfectCrime`.

2b. Alternatively, if you're using the Git command line client, navigate to the directory you want your Unity project to live, for example:

`cd /users/default/documents`

and run the following command:

`git clone https://userid@bitbucket.org/themongs/the-perfect-crime-unity-project.git`

(Don't forget to replace the URL with yours above, so you have the correct userid).

Both these actions will probably require your Bitbucket.org password.

### Open the project in Unity

* Start Unity, click "Open" on the projects screen and select the folder that Git cloned the project into.

* If you get a notice about a different editor version, just click "OK".

### Configure your Git

* You'll probably want to change your Git author name and email, so in SourceTree go Tools; Options.

* Fill out the Default user information to contain your real name and your RMIT email address.

Alternatively, if you use the Git CLI, do the following:

```
git config user.name "My Name"
git config user.email "user@email"
```

## Making changes

* Ensure your git client is on the `master` branch, then pull the latest changes from Bitbucket.

* Create a new branch, preferably one that starts with your name so we can tell who's is who. It's easier to keep track of things if you make a new branch for each feature, but if you want to work on a single branch that's just your name that's fine too.

* Make your changes and test that everything still works. Where possible try and avoid introducing warnings into the Unity console.

* Stage + commit the files using SourceTree.

* Push your new branch (you must tick the box for each new branch in the push dialog) so others can see it.

* If you start on a new feature not dependent on your old one, start this list from the top again (that is, go back to master, pull, and branch again).

## Building the finished product

* Open the game folder in Unity Editor.

* Select "Build Settings" from the "File" menu, then click "Build" and select a target destination.

