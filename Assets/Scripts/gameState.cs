﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class gameState : MonoBehaviour {
	public enum Difficulty:int { Easy, Medium, Hard };
	public int scoreRight = 0;
	public int scoreWrong = 0;
	public bool won = false;
    public bool surrender = false;
	public Difficulty difficulty = Difficulty.Easy;
	public static gameState _instance;

	void Awake () {
		// There can be only one
		if (!_instance)
			_instance = this;
		else
			Destroy (this);
	}

	// Create the gamestate object if it doesn't already exist,
	// and return the actual instance of the gameState object.
	public static gameState getGameState()
	{
		GameObject stateContainer = null;
		if (FindObjectOfType(typeof(gameState)) == null) {
			stateContainer = new GameObject ("GameState");
			stateContainer.AddComponent<gameState> ();
			GameObject.DontDestroyOnLoad (stateContainer);
		}

		return GameObject.Find("GameState").GetComponentInChildren<gameState>();
	}

	public int scoreLoseThreshold { 
		get {
			switch (difficulty) {
			case gameState.Difficulty.Easy:
				return 20;
			case gameState.Difficulty.Medium:
				return 15;
			default:
				return 10;
			}
		}
	}

	public int scoreWinThreshold { 
		get {
			switch (difficulty) {
			case gameState.Difficulty.Easy:
				return 3;
			case gameState.Difficulty.Medium:
				return 4;
			default:
				return 5;
			}
		}
	}
}
