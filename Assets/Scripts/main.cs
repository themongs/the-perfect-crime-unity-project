﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class UIWidgets {
	public Text screen;
	public GameObject buttonGroup;

	public Button getButton(int b) {
		Button[] buttons;

		buttons = buttonGroup.GetComponentsInChildren<Button> ();

		// Only allow us to address the first 9 buttons
		if (b > 9 || b < 0)
			throw new Exception ("Invalid button address");

		// Array is zero-indexed but we use it where 1 is the first object
		return buttons[b - 1];
	}

	public virtual void setButtonText(int i, string text) {
		Button button = getButton (i);

		button.GetComponentInChildren<Text> ().text = text;
	}
}

public class main : MonoBehaviour {
	private game g = new game();
	private gameState state;
	public UIWidgets ui;

	void Start() {
		// Fetch the game state manager.
		state = gameState.getGameState();
        state.surrender = false;
		Debug.Log ("Difficulty: " + state.difficulty);

		// Point the interrogation class at our UI.
		g.setUI(ui);
		g.setState (state);

		// Read the various data files in, and shuffle the data
		g.LoadCharacters (openDataFile("characters"));
		g.LoadFurniture (openDataFile ("furniture"));
		g.LoadClues (openDataFile ("clues"));
		g.LoadWeapons (openDataFile ("weapons"));
		g.LoadRooms (openDataFile ("rooms"));

		// Fill the Rooms with other objects
		g.SetupFurniture ();
		g.SetupMurder ();
		g.SetupItems ();
		g.SetupWitnesses ();

		// Spew out debug information to console.
		g.printAllCharacters ();
		//g.printAllFurniture ();
		//g.printAllItems ();
		//g.printAllRooms();

		// Display the intro with the scenario
		ui.screen.text = g.i.introMurderScenario ();
	}

	// Target method for answer buttons
	// - pass the answer on to the interrogation object
	public void btnAnswer(int btn) {
		g.i.answer(btn);
	}

	// Construct a StreamReader object pointing to the text file of the given filename
	private StreamReader openDataFile(string filename) {
		TextAsset textdata = Resources.Load (filename) as TextAsset;
		MemoryStream ms = new MemoryStream (textdata.bytes);
		StreamReader reader = new StreamReader (ms, true);
		return reader;
	}

    public void btnSurrenderPressed()
    {
        //Gamestate condition is Quit
        state.surrender = true;
        state.won = false;
        SceneManager.LoadScene("scnGameOver");
    }
}
	