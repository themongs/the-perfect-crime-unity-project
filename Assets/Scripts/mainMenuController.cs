﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainMenuController : MonoBehaviour {
	gameState state;

	public void Start() {
		// Fetch the game state manager.
		state = gameState.getGameState();

		// If we only have the background loaded, fill it in with the main menu.
		if (SceneManager.GetActiveScene().name == "scnMenuBackground" && 
			SceneManager.sceneCount == 1)
			SceneManager.LoadSceneAsync ("scnMainMenu", LoadSceneMode.Additive);

		// Handle the Game Over business
		if (SceneManager.GetActiveScene ().name == "scnGameOver") {
            //Surrender Condition
            if (state.surrender)
            {
				GameObject.Find("textWinLose").GetComponentInChildren<Text>().text = "Murderer!".ToUpper();
				GameObject.Find("textContent").GetComponentInChildren<Text>().text = "You  have  admitted  your  crime  and  accept  your  punishment.".ToUpper();
                GameObject.Find("bgLose").GetComponentInChildren<Image>().enabled = true;
                GameObject.Find("bgWin").GetComponentInChildren<Image>().enabled = false;
            }
            //Lose Condition
			else if (!state.won) {
				GameObject.Find("textWinLose").GetComponentInChildren<Text>().text = "Uh oh!".ToUpper();
				GameObject.Find("textContent").GetComponentInChildren<Text>().text = "The  detective  is  pretty  sure  you  did  it.".ToUpper();
                GameObject.Find("bgLose").GetComponentInChildren<Image>().enabled = true;
                GameObject.Find("bgWin").GetComponentInChildren<Image>().enabled = false;
            }
            //Win Condition
            else
            {
				GameObject.Find("textWinLose").GetComponentInChildren<Text>().text = "Congratulations".ToUpper();
				GameObject.Find("textContent").GetComponentInChildren<Text>().text = "You  have  successfully  blamed  the  murder  on  someone  else.".ToUpper();
                GameObject.Find("bgLose").GetComponentInChildren<Image>().enabled = false;
                GameObject.Find("bgWin").GetComponentInChildren<Image>().enabled = true;
            }
            

        }
	}

    //Main Menu Buttons
    public void btnNewGamePressed()
    {
		SceneManager.UnloadScene ("scnMainMenu");
		SceneManager.LoadSceneAsync("scnDifficulty", LoadSceneMode.Additive);
    }

    public void btnCreditsPressed()
    {
		SceneManager.UnloadScene ("scnMainMenu");
		SceneManager.LoadSceneAsync("scnCredits", LoadSceneMode.Additive);
    }

    //Difficulty Menu Buttons
    public void btnEasyPressed()
    {
		gameState state = GameObject.Find("GameState").GetComponentInChildren<gameState>();
		state.difficulty = gameState.Difficulty.Easy;

        SceneManager.LoadScene("scnGameplay");
    }

    public void btnMediumPressed()
    {
        gameState state = GameObject.Find("GameState").GetComponentInChildren<gameState>();
        state.difficulty = gameState.Difficulty.Medium;

        SceneManager.LoadScene("scnGameplay");
    }

    public void btnHardPressed()
    {
        gameState state = GameObject.Find("GameState").GetComponentInChildren<gameState>();
        state.difficulty = gameState.Difficulty.Hard;

        SceneManager.LoadScene("scnGameplay");
    }

    //Universal Buttons
    public void btnBackPressed()
    {
		// We could probably scatter-shot unload the other menus and not re-load
		// this scene, but it's not terribly expensive and this makes sure
		// we get back to a known-good state.
		SceneManager.LoadScene("scnMenuBackground");
    }

    //Application Buttons
    public void btnQuitPressed()
    {
		// This does nothing in the Unity editor.
        Application.Quit();
    }
}
