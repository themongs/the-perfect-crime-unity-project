﻿using UnityEngine;
using System.Collections;

public class character{
    private string charName;
    private string charRole;
    private string hairColour;
    private string eyeColour;
    private string nationality;
	private string special;

    private int charAgeYears;
    private int charHeightCm;

    private string subjectiveHeight;
    private string subjectiveAge;

    //male = true, female = false
    //sorry, just binary gender here :( nothing inferred by having men as true or woman as false, purely arbitrary...
    private bool gender;

    private bool guilty;
    private bool victim;

	public character (string nameIn, char genderIn, string roleIn, string hairIn, string eyeIn, 
		string nationIn, int ageIn, int heightIn, string specialIn) {
        charName = nameIn;
        charRole = roleIn;
        hairColour = hairIn;
        eyeColour = eyeIn;
        nationality = nationIn;
        charAgeYears = ageIn;
        charHeightCm = heightIn;
		special = specialIn;
        guilty = false;
        victim = false;

        //gender is used to assist gameplay, e.g. if a character says "I heard a MALE and a FEMALE voice arguing in the murder room"
        //if we know the victim is FEMALE, then the murder must be MALE
        //likewise, if a character says "I heard 2 MALES arguring in the murder room"
        //if we know the victim is MALE, then the murderer must be MALE as well
        gender = (genderIn == 'm');

        subjectiveAge =  (ageIn <= 25 ) ? "young" : (ageIn < 50 ) ? "middle-aged" : "old";
        //subjective heights reported by characters 150 to 159 = short, 160 - 169 = average, 170 - 180 tall.
        //when a character says they saw a "tall person" leave the room, it could be anyone fitting the 170 - 180 critera.
        //good way to insinuate another tall character was there at the crime
        subjectiveHeight = (heightIn < 160) ? "short" : (heightIn < 170) ? "average" : "tall";
    }

    public string getName{
        get
        {
            return charName;
        }
        //should not change, hence no set function
    }

    public string getFirstName
    {
        get
        {
            // Hackish.
            string[] names = charName.Split (' ');
            return names[0];
        }
        //should not change, hence no set function
    }

    public string getLastName
    {
        get
        {
            // Hackish.
            string[] names = charName.Split (' ');
            return names[1];
        }
        //should not change, hence no set function
    }

    public string getRole
    {
        get
        {
            return charRole;
        }
        //should not change, hence no set function
    }

    public string getHair
    {
        get
        {
            return hairColour;
        }
    }

    public string getEyes
    {
        get
        {
            return eyeColour;
        }
        //should not change, hence no set function
    }

    public string getNationality
    {
        get
        {
            return nationality;
        }
        //should not change, hence no set function
    }

    public int getAge
    {
        get
        {
            return charAgeYears;
        }
        //should not change, hence no set function
    }

    public int getHeight
    {
        get
        {
            return charHeightCm;
        }
        //should not change, hence no set function
    }

    public string getSubjectiveHeight
    {
        get
        {
            return subjectiveHeight;
        }
        //should not change, hence no set function
    }

    public string getSubjectiveAge
    {
        get
        {
            return subjectiveAge;
        }
        //should not change, hence no set function
    }

    public bool getVictim
    {
        get
        {
            return victim;
        }
        set
        {
            victim = value;
        }
    }

    public bool getGuilty
    {
        get
        {
            return guilty;
        }
        set
        {
            guilty = value;
        }
    }

    public bool getGender
    {
        get
        {
            return gender;
        }
        //should not change, hence no set function
	}

	public string getSpecial
	{
		get
		{
			return special;
		}
		//should not change, hence no set function
	}

	public string getPronounObjective() {
		if (gender)
			return "him";
		else
			return "her";
	}

	// If you're going to use these mid-sentence, use ToLower() on them.
	public string getPronounSubjective() {
		if (gender)
			return "He";
		else
			return "She";
	}

	public string getPronounPossessive() {
		if (gender)
			return "his";
		else
			return "her";
	}

	//return room.Find("Main Camera").GetComponent<room>();
}
