﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

public class interrogation {
	private gameState state;
	private game g;
	private UIWidgets ui;

	//part of the interrogator questions
	//each number corrosponds to HEIGHT, EYES, HAIR, GENDER, NATIONALITY,
	private String[] characteristic = new String[]{ "voice", "accent", "eyes", "hair", "stature" };
	private enum questionType:int { Response=0, SingleTrait=1, DoubleTrait=2, Item=3, Weapon=4, Witness=5, WitnessSub=6 };

	private int characteristicIndex = 0;
	private int itemIndex = 0;
	private int witnessIndex = 1;
	private questionType answerType = questionType.Response;

	// Scoring data
	private int scoreRight;
	private int scoreWrong;

	public interrogation(game parent) {
		g = parent;
	}

	public void setUI(UIWidgets u) {
		ui = u;
	}

	public void setState(gameState gState) {
		state = gState;
	}


	/*  Oppurtunities to accuse other players based on matching characteristics or clues

            1. Single Characteristic accusations (much more likley player can use to accuse)
                    One of the witnesses saw a "SUBJECTIVE HEIGHT" person enter the "MURDER SCENE" just before.
                    One of the witnesses saw a person with "EYES" eyes flee the "MURDER SCENE"
                    One of the witnesses saw a person with "HAIR" hair flee the "MURDER SCENE"
                    One of the witnesses heard a "GENDER voice in the "MURDER SCENE"
                    One of the witnesses heard a person with "NATIONALITY" accent in the "MURDER SCENE"

            2. Double Characteristic based accusations:                    
                    One of the witnesses heard a "GENDER" voice, with a "Nationality accent" in the "MURDER SCENE"
                    One of the witnesses saw a "TALL" person with "HAIR" hair flee the "MURDER SCENE" at 11:01pm.

                    NOTE: (very hard to accuse other player, maybe impossible. BUT! if you do accuse another player and it is wrong, 
                    the detective will tell you why, and they will describe that character so that you can use their details later on)

            3. COMBINED Character VS Character accusations && Single Characteristics accusations
                    CHARACTER heard a "GENDER voice in the "MURDER SCENE"
                    CHARACTER heard a person with "NATIONALITY" accent in the "MURDER SCENE"
                    CHARACTER saw a person with "EYES" eyes flee the "MURDER SCENE"
                    CHARACTER saw a person with "HAIR" hair flee the "MURDER SCENE"
                    CHARACTER saw a "SUBJECTIVE HEIGHT" person enter the "MURDER SCENE" just before.

                    NOTE: has all the possiblities of Single Characteristic accusations BUT the CHARACTER accusing you, may have a feature which will discredit them.
                    witness CHARACTER wears glasses = couldnt see properly with VISION based questions
                    witness CHARACTER has a hearing aid = couldnt hear properly with HEARING based questions

            4. COMBINED Character VS Character accusations && DOUBLE Characteristics accusations
                    CHARACTER heard a "GENDER" voice, with a "Nationality accent" in the "MURDER SCENE"
                    CHARACTER saw a "TALL" person with "HAIR" hair flee the "MURDER SCENE" at 11:01pm.

                    NOTE: multiple combinations of character characteristics = much harder, and sometimes impossible for the player to accuse another player.
                    BUT: there is also the possible benefit and escape of "Character VS Character accusations" where the player might be able to discredit the witness
                    in this case, the player does not find out any details of other characters, but they get to avoid the question (especially handy if it's impossible to accuse someone else).

            5. Clue based accusations reffering to murder suspect characteristics:
                    A "NATIONALITY CURRENCY:clue" was found left at the crime scene, and you are "NATIONALITY" (e.g. russian rubles, match russian nationality)
                    The murder weapon was a "MURDER WEAPON:weapon", and yet you are a "OCCUPATION". It would seem an unlikely coincidence... (Match gun with soldier, or saucepan with chef)
                    A "CLUE" was found at the murder scene, and yet you are "AGE" years old. The two seem to be related, wouldn't you say? (match "pension card clue" with "old age", or student card with "young age", "business card" with "middle age" 
         */

	// Handle answer button clicks.
	public void answer(int answer)
	{
		if (answerType == questionType.Response) {
			// We're at "next question" screen, user has just pressed "next question", 
			// so figure out which question to ask next.
			nextQuestion();
			return;
		}

		// We're answering an actual question.
		switch (answerType)
		{
		case questionType.SingleTrait:
			ui.screen.text = responseTraits(answer);
			characteristicIndex++;
			break;
		case questionType.DoubleTrait:
			ui.screen.text = responseTraits (answer);
			characteristicIndex += 2;
			break;
		case questionType.Item:
			ui.screen.text = responseItems(answer);
			itemIndex++;
			break;
		case questionType.Weapon:
			ui.screen.text = responseWeapons(answer);
			itemIndex++;
			break;
		case questionType.Witness:
			ui.screen.text = responseWitness (answer);
			if (answer == 9) { // Q5 subquestion
				answerType = questionType.WitnessSub;
				allNamesOnButtons ();
				return;
			}
			witnessIndex++;
			break;
		case questionType.WitnessSub:
			ui.screen.text = responseWitnessSub (answer);
			witnessIndex++;
			break;
		default:
			Debug.Log ("Uncaught answer: " + answer);
			break;
		}

		// Check for win/loss condition
		Debug.Log(string.Format("Score: {0} right, {1} wrong.", scoreRight, scoreWrong));
		if (scoreRight >= state.scoreWinThreshold) {
			state.won = true;
			state.scoreRight = scoreRight;
			state.scoreWrong = scoreWrong;
			SceneManager.LoadScene ("scnGameOver");
		} else if (scoreWrong >= state.scoreLoseThreshold) {
			state.won = false;
			state.scoreRight = scoreRight;
			state.scoreWrong = scoreWrong;
			SceneManager.LoadScene ("scnGameOver");
		}

		// Allow the user to acknowledge the response before moving on to the next question.
		answerType = questionType.Response;
		allNextQuestionOnButtons();
	}

	// Figure out what question to ask next. 
	public void nextQuestion()
	{
		int qnum;
		
		// If we're out of question type 1/2 questions, set it so we can't
		if (characteristicIndex >= characteristic.Length)
			characteristicIndex = 0;

		// If we're out of type3 questions, set it so we can't get that one
		if (itemIndex > 4)
			itemIndex = 0;

		// On the off chance we get through all the witness questions, cycle back through them.
		if (witnessIndex > 9)
			witnessIndex = 1;

		// Are we on the subquestion for the witness question?
		if (answerType == questionType.WitnessSub) {
			// The UI is updated in the response to this question, so put names on buttons and stop.
			allNamesOnButtons();
			return;
		}

		qnum = g.getRNG().Next(0, 4);

		switch (qnum) {
		case 0: // Characteristic-based questions.
			goto case 1;
		case 1:
			// As long as we're not at the end of the list of characteristic questions, we have
			// a 33% chance of getting a question with two characteristics. Very difficult.
			if ((characteristicIndex < characteristic.Length - 1) && g.getRNG ().Next (0, 3) == 1) {
				ui.screen.text = interrogateDoubleCharacteristic ();
				allNamesOnButtons ();
				answerType = questionType.DoubleTrait;
				return;
			}

			// Otherwise, it's single characteristic
			ui.screen.text = interrogateSingleCharacteristic ();
			allNamesOnButtons ();
			answerType = questionType.SingleTrait;
			return;

		case 2: // Item-related questions.
			// If the item at itemIndex is a weapon, ask about a weapon.
			if (g.getFurnitureAt(g.getMurderRoom.getFurniture [itemIndex]) == g.getHidingPlace)
			{
				ui.screen.text = interrogateWeapon();
				allNamesOnButtons();
				answerType = questionType.Weapon;
				return;
			}

			// Otherwise, ask about an item
			ui.screen.text = interrogateItem ();
			allNamesOnButtons ();
			answerType = questionType.Item;
			return;
		
		case 3: // Witness question
			answerType = questionType.Witness;
			ui.screen.text = interrogateWitness();
			witnessExcusesOnButtons ();
			return;
		}

		Debug.Log ("Couldnt find a question, help!");
	}

	public string introMurderScenario()
	{
		StringBuilder builder = new StringBuilder();
		character player = g.getPlayerCharacter;
		character victim = g.getVictimCharacter;
		room murderRoom = g.getMurderRoom;

		builder.Append(string.Format("Your name is {0} and you are {1} {2}. ", player.getName, aOrAn(player.getRole), player.getRole));
		builder.Append("The following is a description of your character:\n");
		builder.Append("\n");
		builder.Append(string.Format("Age: {0} ({1})\n", player.getAge, player.getSubjectiveAge));
		builder.Append(string.Format("Height: {0} ({1})\n", player.getHeight, player.getSubjectiveHeight));
		builder.Append(string.Format("Eyes: {0}\n", player.getEyes));
		builder.Append(string.Format("Hair: {0}\n", player.getHair));
		builder.Append(string.Format("Nationality: {0}\n", player.getNationality));
		builder.Append(string.Format("Unique: {0}\n", player.getSpecial));
		builder.Append("\n");
		builder.Append(string.Format("At {0}pm you killed {1} the {2} in the {3}.\n", 
			11, 
			victim.getName, 
			victim.getRole, 
			murderRoom.getName));
		builder.Append(string.Format("You did it with a {0} due to an argument you both had over money.\n", 
			g.getMurderWeapon.getItemName));
		builder.Append("Since there was a party in the dining room, no one heard a sound.\n");
		builder.Append(string.Format("You then left the {0} by the {1} in the {2}.\n",
			g.getMurderWeapon.getItemName,
			g.getHidingPlace.getItemName,
			murderRoom.getName));
		builder.Append("\n");
		builder.Append("All suspects who were there on the night have been questioned, and you are now the last one.\n");
		builder.Append("You must evade the detective’s questions, but also use their lack of knowledge to accuse another character of the murder.\n");
		builder.Append("You can do this by referring to additional evidence to convince the detective someone else did it.\n");
		builder.Append("Good luck... killer.\n");

		allNextQuestionOnButtons ();
		return builder.ToString();
	}

	public string interrogateSingleCharacteristic()
	{
		/*1.Single Characteristic accusations (much more likley player can use to accuse)
                One of the witnesses HEARD a person with a "GENDER" voice at the "MURDER SCENE"
                One of the witnesses HEARD a person with a "NATIONALITY" accent in the "MURDER SCENE"
                One of the witnesses SAW a person with "EYES" eyes at the "MURDER SCENE"
                One of the witnesses SAW a person with "HAIR" hair at the "MURDER SCENE"
                One of the witnesses SAW a peron of "SUBJECTIVE HEIGHT" at the "MURDER SCENE"*/

		string trait = "";
		character player = g.getPlayerCharacter;
		StringBuilder builder = new StringBuilder();
		switch (characteristicIndex)
		{
		case 0:
			trait = "a " + ((player.getGender) ? "male" : "female");
			break;
		case 1:
			trait = aOrAn (player.getNationality) + " " + player.getNationality;
			break;
		case 2:
			trait = player.getEyes;
			break;
		case 3:
			trait = player.getHair;
			break;
		case 4:
			trait = player.getSubjectiveHeight;
			break;
		}

		builder.Append("Interrogator:\n");
		builder.Append("\n");

		switch (g.getRNG ().Next (0, 3)) {
		case 0:
			builder.Append (string.Format ("One of the witnesses {0} a person {1} {2} {3} in the {4}\n",
				((characteristicIndex < 2) ? "heard" : "saw"),
				((characteristicIndex != 4) ? "with" : "of"),
				trait,
				characteristic [characteristicIndex],
				g.getMurderRoom.getName));
			builder.Append ("\n");
			builder.Append ("This fits your description. Care to explain?\n");
			break;
		case 1:
			builder.Append (string.Format ("I'm told there was someone {0} {1} {2} present, what do you think about that?\n",
				((characteristicIndex != 4) ? "with" : "of"),
				trait,
				characteristic [characteristicIndex]));
			break;
		case 2:
			builder.Append (string.Format ("Someone {0} a person {1} {2} {3} in the {4}, and I think it was you.\n",
				((characteristicIndex < 2) ? "heard" : "saw"),
				((characteristicIndex != 4) ? "with" : "of"),
				trait,
				characteristic [characteristicIndex],
				g.getMurderRoom.getName));
			break;
		}
		builder.Append("\n");
		builder.Append(string.Format("Player {0}:\n", player.getName));
		builder.Append("\n");
		switch (g.getRNG ().Next (0, 3)) {
		case 0:
			builder.Append("Although that sounds like me, it was probably...\n");
			break;
		case 1:
			builder.Append ("Look it's a misunderstanding. For instance, look at...\n");
			break;
		case 2:
			builder.Append ("Have you considered...\n");
			break;
		}

		return builder.ToString();
	}

	public string interrogateDoubleCharacteristic()
	{
		//NOTE!!! characteristicIndex MUST BE LESS THAN characteristicIndex.Length-1
		//Otherwise array out of bounds exception

		/*2.Double Characteristic based accusations:
                    One of the witnesses heard a "GENDER" voice, with a "Nationality accent" in the "MURDER SCENE"
                    One of the witnesses saw a "TALL" person with "HAIR" hair flee the "MURDER SCENE" at 11:01pm.

                    NOTE: (very hard to accuse other player, maybe impossible. BUT! if you do accuse another player and it is wrong, 
                    the detective will tell you why, and they will describe that character so that you can use their details later on)*/

		StringBuilder builder = new StringBuilder();
		string trait = "";
		string trait2 = "";
		character player = g.getPlayerCharacter;
		int randomCharacter = 3; // Chosen by fair dice roll, guaranteed to be random.

		switch (characteristicIndex)
		{
		case 0:
			trait = "a " + (player.getGender ? "male" : "female");
			break;
		case 1:
			trait = aOrAn (player.getNationality) + " " + player.getNationality;
			break;
		case 2:
			trait = player.getEyes;
			break;
		case 3:
			trait = player.getHair;
			break;
		case 4:
			trait = player.getSubjectiveHeight;
			break;
		}
		switch (characteristicIndex + 1)
		{
		case 0:
			trait2 = "a " + (player.getGender ? "male" : "female");
			break;
		case 1:
			trait2 = aOrAn (player.getNationality) + " " + player.getNationality;
			break;
		case 2:
			trait2 = player.getEyes;
			break;
		case 3:
			trait2 = player.getHair;
			break;
		case 4:
			trait2 = player.getSubjectiveHeight;
			break;
		}

		builder.Append("Interrogator:\n");
		builder.Append("\n");
		builder.Append(string.Format("{0} said {1} {2} a person {3} {4} {5} in the {6}.\n",
			(g.getCharacterAt(randomCharacter).getName),
			g.getCharacterAt(randomCharacter).getPronounSubjective().ToLower(),
			((characteristicIndex < 2) ? "heard" : "saw"),
			((characteristicIndex != 4) ? "with" : "of"),
			trait,
			characteristic[characteristicIndex],
			g.getMurderRoom.getName));
		builder.Append("\n");
		builder.Append(string.Format("{0} also mentioned this person {1} {2} {3}.\n",
			g.getCharacterAt(randomCharacter).getPronounSubjective(),
			((characteristicIndex + 1 != 4) ? "had" : "was of"),
			trait2,
			characteristic[characteristicIndex + 1]));
		builder.Append("\n");
		builder.Append("This fits your description. Care to explain?\n");
		builder.Append("\n\n");
		builder.Append(string.Format("Player {0}:\n", player.getName));
		builder.Append("\n");
		builder.Append("Although that sounds like me, it was probably...\n");

		return builder.ToString();
	}

	public string interrogateItem()
	{
		StringBuilder builder = new StringBuilder();
		character player = g.getPlayerCharacter;
		character victim = g.getVictimCharacter;

		itemFurniture furniture = g.getFurnitureAt (g.getMurderRoom.getFurniture [itemIndex]);
		itemGeneric item = g.getItemAt (furniture.getItem);

		builder.Append("Interrogator:\n");
		builder.Append("\n");
		builder.Append(string.Format("In the {0} where the victim was found, police also discovered ",
			g.getMurderRoom.getName));
		builder.Append(string.Format(
			item.getItemDescription,

			player.getName,                         //{0}
			victim.getName,                         //{1}
			player.getRole,                         //{2}
			player.getNationality,                  //{3}
			player.getGender ? "woman" : "man",     //{4}
			(player.getFirstName)[0],               //{5}
			(player.getLastName)[0],                //{6}
			player.getLastName,                     //{7}
			player.getGender ? "Sally" : "Geoff",   //{8}
			player.getFirstName,                    //{9}
			player.getGender ? "Mrs." : "Mr.",      //{10}
			player.getGender ? "Mr." : "Mrs.",      //{11}
			player.getSubjectiveHeight				//{12}
		));

		builder.Append("\n\n");
		builder.Append("Going by your profile, this seems to belong to you. Care to explain?\n");
		builder.Append("\n");
		builder.Append(string.Format("Player {0}:\n", player.getName));
		builder.Append("\n");
		builder.Append("It's just a simple misunderstanding, that probably belongs to...\n");

		return builder.ToString ();
	}

	public string interrogateWeapon()
	{
		StringBuilder builder = new StringBuilder();

		builder.Append("Interrogator:\n");
		builder.Append("\n");
		builder.Append(string.Format("The victim's death was believed to be caused by {0}.\n",
			g.getMurderWeapon.getAction));
		builder.Append("\n");
		builder.Append(string.Format("Police also found {0} {1} in the {2} where you were last seen.\n\nThey believe this {1} could have been used to kill {3}.\n",
			aOrAn(g.getMurderWeapon.getItemName),
			g.getMurderWeapon.getItemName,
			g.getMurderRoom.getName,
			g.getVictimCharacter.getName));
		builder.Append("\n");
		builder.Append(string.Format("They described the {1} as {0}\n",
			g.getMurderWeapon.getItemDescription,
			g.getMurderWeapon.getItemName));
		builder.Append("\n");
		builder.Append("Care to explain?\n");
		builder.Append("\n");
		builder.Append(string.Format("Player {0}:\n", g.getPlayerCharacter.getName));
		builder.Append("\n");
		builder.Append(string.Format("I don't know anything about that, are you sure a similar {0} weapon wasn't in the room of...\n",
			g.getMurderWeapon.getAction));

		return builder.ToString();
	}

	public string interrogateWitness()
	{
		StringBuilder builder = new StringBuilder();

		builder.Append("Interrogator:\n");
		builder.Append("\n");
		builder.Append(string.Format("{0} claims {1} noticed you at the scene of the crime just before the victim was found.\n",
			g.getCharacterAt(witnessIndex).getName,
			g.getCharacterAt(witnessIndex).getPronounSubjective().ToLower()));
		builder.Append("\n");
		builder.Append("Care to explain?\n");
		builder.Append("\n");
		builder.Append(string.Format("Player {0}:\n", g.getPlayerCharacter.getName));
		builder.Append("\n");
		builder.Append(string.Format("Well {0} must be mistaken because they...\n",
			g.getCharacterAt(witnessIndex).getName));

		return builder.ToString();
	}

	public void allNamesOnButtons()
	{
		for (int i = 1; i <=9; i++)
			ui.setButtonText(i, g.getCharacterAt(i).getName);
	}

	public void allNextQuestionOnButtons()
	{
		for (int i = 1; i <= 9; i++)
			ui.setButtonText(i, "Next Question >>>");
	}

	public void witnessExcusesOnButtons()
	{
		string[] responses = {
			"have bad sight.",
			"have bad hearing",
			"can't leave their room",
			"are old & confused",
			"are young & devious",
			"were sleeping",
			"were fixing the plumbing",
			"were washing dishes",
			"it was someone else..."
		};

		for (int i = 1; i <= 9; i++)
			ui.setButtonText (i, responses [i-1]);
	}

	public string responseTraits(int answer)
	{
		Boolean correctAnswer1 = false;
		Boolean correctAnswer2 = false;
		StringBuilder builder = new StringBuilder();

		character player = g.getPlayerCharacter;
		character accused = g.getCharacterAt (answer);

		/*code to check answer here*/
		//String[] characteristic = new String[]{ "voice", "accent", "eyes", "hair", "stature"};
		//1 "voice" = gender, 
		//2 "accent" = nationality, 
		//3 "eyes" = eye colour, 
		//4 "hair" = hair colour, 
		//5 "stature" = height
		switch (characteristicIndex)
		{
		case 0:
			correctAnswer1 = (player.getGender == accused.getGender);
			break;
		case 1:
			correctAnswer1 = ((player.getNationality).Equals ((accused.getNationality), StringComparison.Ordinal));
			break;
		case 2:
			correctAnswer1 = ((player.getEyes).Equals ((accused.getEyes), StringComparison.Ordinal));
			break;
		case 3:
			correctAnswer1 = ((player.getHair).Equals ((accused.getHair), StringComparison.Ordinal));
			break;
		case 4:
			correctAnswer1 = ((player.getSubjectiveHeight).Equals ((accused.getSubjectiveHeight), StringComparison.Ordinal));
			break;
		}

		// If the question is a double-trait question, check the next characteristic as well.
		if (answerType == questionType.DoubleTrait) {
			switch (characteristicIndex + 1) {
			case 0:
				correctAnswer2 = (player.getGender == accused.getGender);
				break;
			case 1:
				correctAnswer2 = ((player.getNationality).Equals ((accused.getNationality), StringComparison.Ordinal));
				break;
			case 2:
				correctAnswer2 = ((player.getEyes).Equals ((accused.getEyes), StringComparison.Ordinal));
				break;
			case 3:
				correctAnswer2 = ((player.getHair).Equals ((accused.getHair), StringComparison.Ordinal));
				break;
			case 4:
				correctAnswer2 = ((player.getSubjectiveHeight).Equals ((accused.getSubjectiveHeight), StringComparison.Ordinal));
				break;
			}
		} else
			correctAnswer2 = true;

		if (correctAnswer1 && correctAnswer2)
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append("I see. I suppose you are correct.\n");
			builder.Append("\n");
			builder.Append("Regardless, it still does not prove that you are innocent.\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...");

			scoreRight++;
		}
		else
		{
            builder.Append("Interrogator:\n");
            builder.Append("\n");
            builder.Append(string.Format("Who, {0}?\n",
                accused.getName));
            builder.Append("\n");
            builder.Append(string.Format("The {0} {1} {2} who {3}{4}{5}, and who also has {6} hair, {7} eyes, and is of {8} height?\n",
                accused.getSubjectiveAge,
                accused.getNationality,
                accused.getRole,
                (accused.getSpecial.Equals("asleep")) ? "was" : "uses",
                (accused.getSpecial.Equals("hearing aid") || accused.getSpecial.Equals("wheelchair")) ? " a " : " ",
                accused.getSpecial,
                accused.getHair,
                accused.getEyes,
                accused.getSubjectiveHeight));
            builder.Append("\n");
			builder.Append("It would seem that this would be impossible\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...\n");

			scoreWrong++;
		}

		return builder.ToString();
	}

	public string responseItems(int answer)
	{
		StringBuilder builder = new StringBuilder();
		character player = g.getPlayerCharacter;
		character victim = g.getVictimCharacter;
		character accused = g.getCharacterAt (answer);

		itemFurniture furniture = g.getFurnitureAt (g.getMurderRoom.getFurniture [itemIndex]);
		itemGeneric item = g.getItemAt (furniture.getItem);

		//assign the clue description with the player's character details to the string "correctAnswer", 
		//then assign the clue description with the details of the player the character is accusing to the string "correctAnswer",
		//compare these strings to see if they match.
		//for example. If the clue description is "a handkerchief embroided with the intialis {5}. {6}."
		//and the player's character's intials are G.F. (George Foreman)
		//and the character the player is accusing also has the intials G.F (Graham Ford)
		//then both strings will match! and the player is then awareded a convince point
		//if they don't match, they are given a suspicion point 
		string playerAnswer = string.Format(
			item.getItemDescription,

			accused.getName,							//{0}
			victim.getName,                				//{1}
			accused.getRole,							//{2}
			accused.getNationality,						//{3}
			accused.getGender ? "woman" : "man",		//{4}
			accused.getFirstName[0],					//{5}
			accused.getLastName[0],						//{6}
			accused.getLastName,						//{7}
			accused.getGender ? "Sally" : "Geoff",		//{8}
			accused.getFirstName,						//{9}
			accused.getGender ? "Mrs." : "Mr.",			//{10}
			accused.getGender ? "Mr." : "Mrs.",         //{11}
			accused.getSubjectiveHeight					//{12}
		);

		string correctAnswer = string.Format(
			item.getItemDescription,

			player.getName,								//{0}
			g.getVictimCharacter.getName,				//{1}
			player.getRole,								//{2}
			player.getNationality,						//{3}
			player.getGender ? "woman" : "man",			//{4}
			(player.getFirstName)[0],					//{5}
			(player.getLastName)[0],					//{6}
			player.getLastName,							//{7}
			player.getGender ? "Sally" : "Geoff",		//{8}
			player.getFirstName,						//{9}
			player.getGender ? "Mrs." : "Mr.",			//{10}
			player.getGender ? "Mr." : "Mrs.",          //{11}
			player.getSubjectiveHeight					//{12}
		);

		if (playerAnswer.Equals(correctAnswer, StringComparison.Ordinal))
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append(string.Format("Hmm, I can see how the {0} could possibly be {1}'s.\n",
				item.getItemName,
				accused.getName));
			builder.Append("\n");
			builder.Append("Regardless, it still does not prove that you are innocent.\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...");

			scoreRight++;
		}
		else
		{
            builder.Append("Interrogator:\n");
            builder.Append("\n");
            builder.Append(string.Format("Who, {0}?\n",
                accused.getName));
            builder.Append("\n");
            builder.Append(string.Format("The {0} {1} {2} who {3}{4}{5}, and who also has {6} hair, {7} eyes, and is of {8} height?\n",
                accused.getSubjectiveAge,
                accused.getNationality,
                accused.getRole,
                (accused.getSpecial.Equals("asleep")) ? "was" : "uses",
                (accused.getSpecial.Equals("hearing aid") || accused.getSpecial.Equals("wheelchair")) ? " a " : " ",
                accused.getSpecial,
                accused.getHair,
                accused.getEyes,
                accused.getSubjectiveHeight));
            builder.Append("\n");
			builder.Append(string.Format("How you could possibly think they own the {0}?\n",
				item.getItemName));
			builder.Append("\n");
			builder.Append("Let's move onto the next question...\n");

			scoreWrong++;
		}

		return builder.ToString();
	}

	public string responseWitness(int answer)
	{
		StringBuilder builder = new StringBuilder();
		Boolean correctAnswer = false;
		Boolean subquestion = false;

		character witness = g.getCharacterAt (witnessIndex);

		switch (answer)
		{
		case 1:
			correctAnswer = (witness.getSpecial.Equals("glasses"));
			break;
		case 2:
			correctAnswer = (witness.getSpecial.Equals("hearing aid"));
			break;
		case 3:
			correctAnswer = (witness.getSpecial.Equals("wheelchair"));
			break;
		case 4:
			correctAnswer = (witness.getAge >= 50);
			break;
		case 5:
			correctAnswer = (witness.getAge <= 25);
			break;
		case 6:
			correctAnswer = (witness.getSpecial.Equals("asleep"));
			break;
		case 7:
			correctAnswer = (witness.getRole.Equals("Plumber"));
			break;
		case 8:
			correctAnswer = (witness.getSpecial.Equals("Maid"));
			break;
		case 9:
			subquestion = true;
			break;
		}

		if (correctAnswer && !subquestion)
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append("I see. I suppose you are correct.\n");
			builder.Append("\n");
			builder.Append("Regardless, it still does not prove that you are innocent.\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...");

			scoreRight++;
		}
		else if (!correctAnswer && !subquestion)
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append(string.Format("Who, {0}?\n",
				witness.getName));
			builder.Append("\n");
			builder.Append(string.Format("The {0} {1} {2} who {3}{4}{5}, and who also has {6} hair, {7} eyes, and is of {8} height?\n",
				witness.getSubjectiveAge,
                witness.getNationality,
				witness.getRole,
				(witness.getSpecial.Equals("asleep"))?"was":"uses",
				(witness.getSpecial.Equals("hearing aid") || witness.getSpecial.Equals("wheelchair")) ?" a " :" ",
				witness.getSpecial,
				witness.getHair,
				witness.getEyes,
				witness.getSubjectiveHeight));
			builder.Append("\n");
			builder.Append("That doesn't quite add up.\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...\n");

			scoreWrong++;
		}

		else if (subquestion)
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append(string.Format("Oh, you think it was someone else?\n"));
			builder.Append("\n");
			builder.Append("Well, who also shares 2 similar features to you?...\n");
		}
		return builder.ToString();
	}

	public string responseWitnessSub(int answer)
	{
		int matchedFeatures = 0;
		character player = g.getPlayerCharacter;
		character accused = g.getCharacterAt (answer);

		matchedFeatures += ((accused.getRole).Equals (player.getRole)) ? 1 : 0;
		matchedFeatures += ((accused.getNationality).Equals (player.getNationality)) ? 1 : 0;
		matchedFeatures += ((accused.getHair).Equals (player.getHair)) ? 1 : 0;
		matchedFeatures += ((accused.getEyes).Equals (player.getEyes)) ? 1 : 0;
		matchedFeatures += ((accused.getSubjectiveHeight).Equals (player.getSubjectiveHeight)) ? 1 : 0;
		matchedFeatures += (accused.getGender == player.getGender) ? 1 : 0;
		matchedFeatures += ((accused.getSpecial).Equals (player.getSpecial)) ? 1 : 0;

		StringBuilder builder = new StringBuilder ();

		if (matchedFeatures >= 2) {
			builder.Append ("Interrogator:\n");
			builder.Append ("\n");
			builder.Append ("I see. I suppose you are correct.\n");
			builder.Append ("\n");
			builder.Append ("Regardless, it still does not prove that you are innocent.\n");
			builder.Append ("\n");
			builder.Append ("Let's move onto the next question...");

			scoreRight++;
		} else {
			builder.Append ("Interrogator:\n");
			builder.Append ("\n");
			builder.Append (string.Format ("You think {0} shares 2 similar features to you?\n",
				accused.getName));
			builder.Append ("\n");
            builder.Append(string.Format("The {0} {1} {2} who {3}{4}{5}, and who also has {6} hair, {7} eyes, and is of {8} height?\n",
                accused.getSubjectiveAge,
                accused.getNationality,
                accused.getRole,
                (accused.getSpecial.Equals("asleep")) ? "was" : "uses",
                (accused.getSpecial.Equals("hearing aid") || accused.getSpecial.Equals("wheelchair")) ? " a " : " ",
                accused.getSpecial,
                accused.getHair,
                accused.getEyes,
                accused.getSubjectiveHeight));
            builder.Append ("\n");
			builder.Append ("It would seem that this would be impossible\n");
			builder.Append ("\n");
			builder.Append ("Let's move onto the next question...\n");

			scoreWrong++;
		}

		return builder.ToString ();
	}

	public string responseWeapons(int answer)
	{
		StringBuilder builder = new StringBuilder();

		Boolean correctAnswer = false;
		room accusedRoom = null;

		int accusedCharWeapon = -1;

		// Search for the accused player's room.
		for (int i = 0; i < 16; i++) {
			room r = g.getRooms () [i / 4, i % 4];
			if (r.getCharacterInRoom == answer) {
				accusedRoom = r;
				break;
			}
		}

		if (accusedRoom == null)
			throw new Exception ("Accused character not in any room. This shouldn't happen.");

		// Step through each of the five items in the room.
		for (int i = 0; i < 5; i++) {
			int accusedItemIndex = g.getFurnitureAt (accusedRoom.getFurniture [i]).getItem;

			// is this item a weapon?
			if (g.getItemAt (accusedItemIndex).GetType () == typeof(itemGenericWeapon)) {
				accusedCharWeapon = g.getFurnitureAt (accusedRoom.getFurniture[i]).getItem;

				// Yes, it's a weapon. Does this weapon have the same action as the murder weapon?
				correctAnswer = ((itemGenericWeapon)g.getItemAt (accusedCharWeapon)).getAction
					.Equals (g.getMurderWeapon.getAction, StringComparison.Ordinal);

				// If the weapons match, quit looking.
				if (correctAnswer) {
					break;
				}
			}
		}

		if (correctAnswer)
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append(string.Format("Yes, there was a {0} found in {1}'s room.\n",
				g.getItemAt(accusedCharWeapon).getItemName,
				g.getCharacterAt(answer)));
			builder.Append(string.Format("It is possible this {0} was used in {1} {2} to death.\n",
				g.getItemAt(accusedCharWeapon).getItemName,
				((itemGenericWeapon)g.getItemAt(accusedCharWeapon)).getAction,
				g.getVictimCharacter.getName));
			builder.Append("\n");
			builder.Append("Regardless, it still does not prove that you are innocent.\n");
			builder.Append("\n");
			builder.Append("Let's move onto the next question...");

			scoreRight++;
		}
		else
		{
			builder.Append("Interrogator:\n");
			builder.Append("\n");
			builder.Append(string.Format("There was no weapon found in {0}'s room that could have caused similar {1} wounds.",
				g.getCharacterAt(answer),
				g.getMurderWeapon.getAction));
			builder.Append("\n");
			builder.Append("Let's move onto the next question...\n");

			scoreWrong++;
		}

		return builder.ToString();
	}

	public static string aOrAn(string word)
	{
		word = word.ToUpper ();
		if (word [0] == 'A' || word [0] == 'E' || word [0] == 'I' || word [0] == 'O' || word [0] == 'U')
			return "an";

		return "a";
	}
}
