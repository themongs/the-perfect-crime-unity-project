﻿using UnityEngine;
using System.Collections;

public abstract class itemGeneric : item
{
    public itemGeneric(string itemNameIn, string itemDescriptionIn) : base(itemNameIn, itemDescriptionIn)
    {
        //there is a itemGeneric class which contains itemWeapons and itemClues 
        //because both weapons and clues should be indistinguishable from each other 
        //as far as the interrogator is concerned (helps make weapons less inconspicuous)

        //itemGeneric's also are owned by (contained within, or rests on) furniture. 
        //Specifically, an itemGenericWeapon "knife" can be found on top of a itemFurniture "rug"
        //or, an itemGenericClue "note" can be found on top of an itemFurniture "couch"
        //or, an itemGenericWeapon "pen" can be found in an itemFurniture "desk"
        //even the floor is considered furniture and can be inspected as such.
    }
}
