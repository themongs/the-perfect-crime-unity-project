﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

public abstract class Loader {
	protected StreamReader reader;
	protected int txtLine;
	protected Random rng;

	public Loader(StreamReader streamReader, Random r) {
		reader = streamReader;
		txtLine = 0;
		rng = r;
	}

	// Read a line from the file and increment line counter
	protected string ReadLine() {
		string line = reader.ReadLine();
		txtLine++;

		if (line == null) {
			throw new EndOfStreamException();
		}

		return line;
	}

	public void setRandomSeed(uint seed) {
		rng = new Random(seed);
	}
}

public class RoomLoader : Loader {
	public RoomLoader(StreamReader sr, Random r) : base (sr, r) {

	}

	public room Next() {
		string line = ReadLine ();
		string[] entries = line.Split ('*');
		int roomType;

		if (entries.Length != 3)
			throw new Exception ("Room Text File Error on line " + txtLine);

		int c = txtLine - 1;

		try {
			roomType = int.Parse(entries [1]);
		}
		catch (System.FormatException) {
			throw new Exception ("Room Text File Error (Integer failed to parse) at line " + txtLine);
		}
			
		if (roomType >= room.getFurnTypeCount || roomType < 1)
			throw new Exception ("Room type out of range at line " + txtLine);

		room r = new room (entries [0], entries [2], c / room.x == 0,   //all northern facing rooms
			c % room.x == (room.x - 1),   //all eastern facing rooms
			c / room.x == (room.y - 1),   //all southern facing rooms
			c % room.x == 0,   //all western facing rooms
			roomType);
		// Currently haven't found a use for windows, but leaving it in for now
		//          N N N N
		//          0 1 2 3
		//          _ _ _ _
		//     W 0 |_|_|_|_| E
		//     W 1 |_|_|_|_| E
		//     W 2 |_|_|_|_| E
		//     W 3 |_|_|_|_| E
		//
		//          S S S S 
		return r;
	}

	public room Copy(room copy) {
		// This is probably a bad idea, will want refactoring later.
		txtLine++;
		int c = txtLine - 1;

		room r = new room (copy.getName, copy.getDescription, c / room.x == 0,   //all northern facing rooms
			c % room.x == (room.x - 1),   //all eastern facing rooms
			c / room.x == (room.y - 1),   //all southern facing rooms
			c % room.x == 0,   //all western facing rooms
			(int)copy.getRoomType);

		return r;
	}
}

public class ClueLoader : Loader {
	public ClueLoader(StreamReader sr, Random r) : base (sr, r) {

	}

	public itemGenericClue Next() {
		string line = ReadLine ();
		string[] entries = line.Split ('*');

		if (entries.Length != 2)
			throw new Exception ("Clue Text File Error on line " + txtLine);

		return new itemGenericClue (entries [0], entries [1]);
	}
}

public class WeaponLoader : Loader {
	public WeaponLoader(StreamReader sr, Random r) : base (sr, r) {

	}

	public itemGenericWeapon Next() {
		string line = ReadLine ();
		string[] entries = line.Split ('*');

		if (entries.Length != 3)
			throw new Exception ("Weapon Text File Error on line " + txtLine);

		return new itemGenericWeapon (entries [0], entries [2], entries [1]);
	}
}

public class FurnitureLoader : Loader {
	public FurnitureLoader(StreamReader sr, Random r) : base (sr, r) {

	}

	public itemFurniture Next() {
		string line = ReadLine ();
		string[] entries = line.Split ('*');
		int furnType;

		if (entries.Length != 3)
			throw new Exception ("Furniture Text File Error on line " + txtLine);

		try {
			furnType = int.Parse(entries[1]);
		}
		catch (System.FormatException) {
			throw new Exception ("Furniture Text File Error (Integer failed to parse) at line " + txtLine);
		}

		// Check if Type is out of bounds
		if (furnType >= room.getFurnTypeCount || furnType < 1)
			throw new Exception("Furniture Text File Error (Integer out of bounds) at line " + txtLine);

		return new itemFurniture(entries[0], entries[2], furnType);
	}
}

public class CharacterLoader : Loader {
	// TODO: Read character traits from files instead of hard-coding.
	private string[] eyes = {"blue", "brown", "hazel", "green"};
	private string[] hair = {"blonde", "brown", "black", "red", "grey"};
	private string[] nationality = {"English", "Chinese", "American", "South African", "Russian", "Japanese" };
	private string[] role = {"Banker", "Computer Analyst", "Plumber", "Maid", "Scientist", 
		"Priest"};
	private string[] special = {"glasses", "hearing aid", "wheelchair", "asleep"};

	public CharacterLoader(StreamReader sr, Random r) : base (sr, r) {
		
	}

	public character Next() {
		string line = ReadLine ();
		string[] entries = line.Split ('*');

		if (entries.Length != 2)
			throw new Exception ("Character Text File Error on line " + txtLine);

		char myGender = entries [1] [0];
		string myHair = hair[rng.Next(0, hair.Length)];
		string myEyes = eyes[rng.Next(0, eyes.Length)];
		string myRole = role[rng.Next(0, role.Length)];
		string myNationality = nationality[rng.Next(0, nationality.Length)];
		int myAge = rng.Next(20, 60);
		int myHeight = rng.Next(150, 180);
		string mySpecial = special [rng.Next (0, special.Length)];

		// Check record integrity
		if (myGender != 'm' && myGender != 'f')
			throw new Exception ("Gender field is neither 'm' or 'f' at line #" + txtLine);

		return new character(entries[0], myGender, myRole, myHair, myEyes, myNationality, myAge, myHeight, mySpecial);
	}

}
