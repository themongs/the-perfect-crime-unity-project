﻿using UnityEngine;
using System.Collections;

public abstract class item {

    //abstract class, cannot use by itself. Have to use with child class.
    protected string itemName;
    protected string itemDescription;

    public item()
    {
        //for testing only.
        itemName = "Error!";
        itemDescription = "Error!";
    }

    public item(string itemNameIn, string itemDescriptionIn)
    {
        itemName = itemNameIn;
        itemDescription = itemDescriptionIn;
    }

    public string getItemName
    {
        get
        {
            return itemName;
        }
        //should not change, hence no set function
    }

    public string getItemDescription
    {
        get
        {
            return itemDescription;
        }
        //should not change, hence no set function
    }
}

