﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class game {
	// Game objects
	private List<character> gameCharacters = new List<character>();
	private List<itemGeneric> gameItems = new List<itemGeneric>();
	private List<itemFurniture> gameFurniture = new List<itemFurniture>();
	private room[,] gameRooms = new room[room.y, room.x];

	private interrogation _interrogation;
	private UIWidgets _ui;

	// These are temporary during setup, permanent objects above.
	private List<itemGenericClue> allClues = new List<itemGenericClue>();
	private List<itemGenericWeapon> allWeapons = new List<itemGenericWeapon>();
	private List<itemFurniture> allFurniture = new List<itemFurniture>();

	// Random number generator
	private Random rng = new Random();

	// Constructor
	public game () {
		_interrogation = new interrogation (this);
	}

	public void setUI(UIWidgets ui) {
		_ui = ui;
		_interrogation.setUI (ui);
	}

	public void setState(gameState gState) {
		_interrogation.setState (gState);
	}

	public interrogation i {
		get {
			return _interrogation;
		}
	}

	public UIWidgets ui {
		get {
			return _ui;
		}
	}

	// Return the character object of the player.
	public character getPlayerCharacter {
		get {
			if (gameCharacters.Count < 2)
				throw new Exception ("Insufficient game characters, is the game set up?");
			
			return gameCharacters [0];
		}
	}

	// Return the character object of the victim.
	public character getVictimCharacter {
		get {
			if (gameCharacters.Count < 2)
				throw new Exception ("Insufficient game characters, is the game set up?");

			character victim = gameCharacters [gameCharacters.Count - 1];
			if (!victim.getVictim)
				throw new Exception ("Victim character isn't dead?!");
			
			return victim;
		}
	}

	// Return the room object of the crime scene.
	public room getMurderRoom {
		get {
			for (int i = 0; i < (room.x * room.y); i++) {
				room r = gameRooms [i / room.x, i % room.x];

				if (r.getMurderRoom == true)
					return r;
			}

			throw new Exception ("No murder room found. Run SetupMurder() first.");
		}
	}

	/** Return the weapon object of the murder weapon.
	 **
	 ** The murder weapon is always first in the gameItems list.
	 */
	public itemGenericWeapon getMurderWeapon { 
		get { 
			return (itemGenericWeapon)gameItems [0]; 
		}
	}

	// Return the furniture object of where the Murder weapon is hidden
	public itemFurniture getHidingPlace {
		get {
			room r = getMurderRoom;

			for (int i = 0; i < room.furnMax; i++) {
				itemFurniture f = gameFurniture [r.getFurniture [i]];
				if (f.getItem == 0) {
					return f;
				}
			}

			throw new Exception ("Murder weapon wasn't found in the murder room.");
		}
	}

	// Return character object of character at index
	public character getCharacterAt(int index) {
		if (index >= gameCharacters.Count)
			throw new Exception ("Index out of bounds fetching character.");

		return gameCharacters [index];
	}

	// Return a furniture object at index
	public itemFurniture getFurnitureAt(int index) {
		if (index >= gameFurniture.Count)
			throw new Exception ("Index out of bounds fetching furniture.");

		return gameFurniture [index];
	}

	// Return a item object at index
	public itemGeneric getItemAt(int index) {
		if (index >= gameItems.Count)
			throw new Exception ("Index out of bounds fetching item.");

		return gameItems [index];
	}

	/** Setup furniture in the rooms.
	 ** 
	 ** Iterate through each room in the gameRooms array and randomly select five
	 ** pieces of relevant (by furnitureType) furniture to populate the room with.
	 ** 
	 ** This code expects the gameRooms array to be populated with Room objects,
	 ** so it must not run before LoadRooms(). It also expects allFurniture to be
	 ** loaded with data from the data file, where it makes copies of these objects 
	 ** to go into each room.
	 */
	public void SetupFurniture() {
		// Make sure we have furniture
		if (allFurniture.Count < 1)
			throw new Exception ("No furniture loaded!");
		
		// Put furniture in rooms.
		for (int i = 0; i < (room.x * room.y); i++) {
			room r = gameRooms[i / room.x, i % room.x];
			int roomType = (int)r.getRoomType;
			int itemCount = 0;
			List<int> roomFurniture = new List<int>();

			// Repeat until we fill the list with five items of furniture relevant to the room type.
			while (itemCount < room.furnMax) {
				// Select a random piece of furniture from the array
				int index = rng.Next (0, allFurniture.Count);
				itemFurniture f = allFurniture [index];

				// If it's suitable for the room we're in, use it, otherwise repeat the loop
				// to get another piece. This is where infinite loops happen if there isn't one
				// piece per room type, which is checked in LoadFurniture().
				if (f.getFurnitureType == roomType) {
					// Copy the furniture item.
					itemFurniture n = new itemFurniture(f.getItemName, f.getItemDescription, f.getFurnitureType);

					gameFurniture.Add (n);
					roomFurniture.Add (gameFurniture.IndexOf(n));

					itemCount++;
				}
			}

			// Add the furniture index to the room object and move on to next room.
			r.getFurniture = roomFurniture.ToArray();
		}
	}

	/** Populate the rooms with the remaining characters.
     **
     ** We skip the first character, as it's the player. They were obviously in the room
     ** with the murder victim, but if the cops could prove that there wouldn't be a game.
     ** Skip the last character, as it's the victim, and they've already been assigned a 
     ** random room.
     ** 
     ** There can only be one character per room, so we check if the room is empty. We simply
     ** dump the characters in the rooms in the order of the array - randomizing it is
     ** probably not worth the effort, as this isn't an RPG.
     **
     ** This function expects gameCharacters and gameRooms to be populated with objects,
     ** so it must not be run before LoadCharacters and LoadRooms(). SetupMurder() will also
     ** potentially clobber a character in a room, so it must run before that too.
     */
	public void SetupWitnesses() {
		// This exception will likely not happen, getMurderRoom() will throw one first.
		if (getMurderRoom == null)
			throw new Exception ("Run SetupMurder() first.");

		// Walk each character *not* including the first or last one.
		for (int i = 1; i < gameCharacters.Count - 1; i++) {
			foreach (room r in gameRooms) {
				if (r.getCharacterInRoom == -1) {
					r.getCharacterInRoom = i;
					break;
				}
			}
		}
	}

	/** Set up the circumstances of the murder.
	 **
	 ** Walk the allItems list and select the first weapon, which we copy
	 ** and set it as the murder weapon.
	 ** 
	 ** Then select a random room, and set it as the murder room, then
	 ** place the murder weapon in a random piece of furniture in the room.
	 ** 
	 ** This function expects gameRooms to be populated, allItems to contain
	 ** at least one weapon, and gameItems to be *empty* as the first gameItem
	 ** is the murder weapon.
	 ** 
	 ** Thus it must be run after LoadRooms(), LoadWeapons(), and SetupFurniture(),
	 ** but before SetupItems() and SetupWitnesses().
	 */
	public void SetupMurder() {
		itemGenericWeapon murderWeapon = null;
		room murderRoom = null;

		// Make sure the game has furniture
		if (gameFurniture.Count < (room.x * room.y * room.furnMax))
			throw new Exception ("Insufficient game furniture. Did you forget to SetupFurniture()?");

		// Make a copy of the first weapon object in the list and call it the murder weapon
		itemGenericWeapon w = allWeapons[0];
		murderWeapon = new itemGenericWeapon(w.getItemName, w.getItemDescription, w.getAction);
		gameItems.Add (murderWeapon);

		// Make sure we got a murder weapon
		if (murderWeapon == null)
			throw new Exception ("Null Murder Weapon - this shouldn't happen.");

		// Pick a random room to put the murder in.
		int rnd = rng.Next(0, (room.x * room.y));
		murderRoom = gameRooms [rnd / room.x, rnd % room.x];
		murderRoom.getMurderRoom = true;
		murderRoom.getCharacterInRoom = gameCharacters.Count - 1;

		// Put the weapon randomly in the first or second piece of furniture
		rnd = rng.Next (2);
		gameFurniture [murderRoom.getFurniture [rnd]].getItem = gameItems.IndexOf (murderWeapon);
	}

	/** Setup items.
	 **
	 ** Iterate through each room, and each piece of furniture in the room,
	 ** and fill each empty piece of furniture with one random item from allItems.
	 ** Making a copy of the item, which could be a weapon or a "clue".
	 **
	 ** This function expects gameRooms to be populated with room objects,
	 ** allItems to have data loaded from the data files, and the murder weapon to
	 ** already have been placed.
	 **
	 ** It should be run after LoadRooms(), LoadClues(), LoadWeapons() and SetupMurder().
	 */
	public void SetupItems() {
		// Error out if we've not loaded any clues.
		if (allClues.Count < 1)
			throw new Exception ("No clues loaded!");

		// Now spread around other items randomly.
		int itemIndex = 0;
		for (int i = 0; i < (room.x * room.y); i++) {
			room r = gameRooms [i / room.x, i % room.x];
			itemFurniture f;
			itemGenericWeapon n;

			// Place a weapon in a random piece of furniture.
			int furnitureSlot = rng.Next (room.furnMax);
			f = gameFurniture[r.getFurniture[furnitureSlot]];
			if (r != getMurderRoom) { // Don't put a second weapon in the murder room.
				n = allWeapons [rng.Next (0, allWeapons.Count)];
				itemGenericWeapon weapon = new itemGenericWeapon (n.getItemName, n.getItemDescription, n.getAction);
				gameItems.Add (weapon);
				f.getItem = gameItems.IndexOf (weapon);
			}

			// Now check each piece of furniture in each room.
			for (int j = 0; j < room.furnMax; j++) {
				f = gameFurniture[r.getFurniture[j]];

				// Check if it's empty first.
				if (f.getItem < 0) {
					// Select a random item from the items list.
					itemGenericClue search = allClues [itemIndex];
					itemGenericClue newItemCopy = new itemGenericClue (search.getItemName, search.getItemDescription);
					gameItems.Add (newItemCopy);
					f.getItem = gameItems.IndexOf (newItemCopy);

					// Increment item counter, roll it over if we run out.
					itemIndex++;
					if (itemIndex >= allClues.Count)
						itemIndex = 0;
				}
			}
		}
	}

	/** Load Characters from the Data file.
	 **
	 ** Pass a StreamReader object pointing to a character data file, which we will
	 ** read line-by-line and create a new character object for each. Then shuffle
	 ** the character list, and the first character becomes the player character
	 ** and the last becomes the victim.
	 **
	 ** Note that our UI only allows for 9 other characters, along with a player character
	 ** and a victim character. That means that when this function is finished, there
	 ** *must* be 11 characters in the list - no more, no less.
	 **
	 ** This function has no pre-requisites other than not expecting to be run twice.
	 */
	public void LoadCharacters(StreamReader reader) {
		CharacterLoader loader = new CharacterLoader (reader, rng);

        if (gameCharacters.Count > 0)
            throw new Exception("Characters already loaded!");

		while (true) {
			try {
				character c = loader.Next ();
				gameCharacters.Add (c);
			} catch (EndOfStreamException) {
				break;
			} catch (Exception e) {
				throw e;
			}
		}

        if (gameCharacters.Count < 11)
            throw new Exception("Not enough characters loaded!");

        // Randomize character order
        gameCharacters = shuffle(gameCharacters);

		// Prune the list to 11 characters.
        while(gameCharacters.Count > 11)
        {
            gameCharacters.RemoveAt(gameCharacters.Count-1);
        }

		// First character is player (murderer)
		gameCharacters[0].getGuilty = true;

		// Last character is victim
		gameCharacters[gameCharacters.Count -1].getVictim = true;
	}

	/** Load the furniture objects from the data file.
	 **
	 ** Pass it a StreamReader pointing to a data file, which it will read line-by-line
	 ** and store in allFurniture. Then shuffle the list of furniture in a random order.
	 ** Finally, check that there is at least one piece of furniture for each room type.
	 **
	 ** This function has no prerequisites.
	 */
	public void LoadFurniture(StreamReader reader) {
		int[] furnTypeTally = new int[room.getFurnTypeCount];
		FurnitureLoader loader = new FurnitureLoader (reader, rng);

		while (true) {
			try {
				itemFurniture f = loader.Next ();
				allFurniture.Add (f);

				// Increment counter
				furnTypeTally[f.getFurnitureType]++;
			} catch (EndOfStreamException) {
				break;
			} catch (Exception e) {
				throw e;
			}
		}

		// Shuffle the furniture list
		allFurniture = shuffle(allFurniture);

		// Confirm we have at least one of each type to avoid infinite loops
		for (int i = 1; i < room.getFurnTypeCount; i++) {
			if (furnTypeTally [i] < 1)
				throw new Exception ("Missing furniture for room type #" + (i+1));
		}
	}

	/** Load clues from data file.
	 **
	 ** Pass it a StreamReader pointing to a data file, which it will read line-by-line
	 ** and add the objects to allItems.
	 **
	 ** This function doesn't really depend on anything.
	 */
	public void LoadClues(StreamReader reader) {
		ClueLoader loader = new ClueLoader (reader, rng);

		while (true) {
			try {
				itemGenericClue c = loader.Next ();
				allClues.Add (c);
			} catch (EndOfStreamException) {
				break;
			} catch (Exception e) {
				throw e;
			}
		}

		allClues = shuffle (allClues);
	}

	/** Load weapons from data file.
	 **
	 ** Pass it a StreamReader pointing to a data file, which it will read line-by-line
	 ** and add the objects to allItems.
	 ** 
	 ** Then shuffle the items list randomly.
	 **
	 ** This function doesn't really depend on anything.
	 */
	public void LoadWeapons(StreamReader reader) {
		WeaponLoader loader = new WeaponLoader (reader, rng);

		while (true) {
			try {
				itemGenericWeapon w = loader.Next ();
				allWeapons.Add (w);
			} catch (EndOfStreamException) {
				break;
			} catch (Exception e) {
				throw e;
			}
		}

		allWeapons = shuffle (allWeapons);
	}

	/** Load rooms from data file.
	 **
	 ** Pass it a StreamReader pointing to a data file, which it will read line-by-line
	 ** and add the objects to gameRooms. It will read at most 16 rooms, and doesn't
	 ** shuffle them so the same 16 rooms will always be used.
	 **
	 ** If the data file does not contain 16 rooms, the rooms will be repeated until
	 ** all 16 cells of the array are filled.
	 */
	public void LoadRooms(StreamReader reader) {
		int txtLine = 0;
		int restartLine = 0;
		RoomLoader loader = new RoomLoader (reader, rng);

		// Iterate through the rooms.txt file, stopping at 16 rooms.
		while (txtLine < (room.x * room.y)) {
			try {
				room r = loader.Next();
				gameRooms[txtLine / room.x, txtLine % room.x] = r;
				txtLine++;
			}
			catch (EndOfStreamException) {
				break;
			}
			catch (Exception e) {
				throw e;
			}
		}

		// If the rooms array isn't full, repeat the rooms.
		restartLine = txtLine;
		while (txtLine < (room.x * room.y)) {
			int i = txtLine - restartLine;
			room r = loader.Copy (gameRooms [i / room.x, i % room.x]);
			gameRooms[txtLine / room.x, txtLine % room.x] = r;
			txtLine++;
		}

	}

	/** Generic list shuffle routine.
	 **
	 ** I thought about trying to implement a proper, smart shuffling algorithm[1] but
	 ** frankly it seemed like too much work for questionable benefit. Our PCs are fast,
	 ** and the dataset is not that big, so simply popping each item out in a random
	 ** order and counting the items left is not prohibitive.
	 ** 
	 ** This function is *destructive*, so don't use it on a read-only List object,
	 ** or something you care about. Most of the time we use it as such:
	 ** 
	 ** List = shuffle(List);
	 **
	 ** So it doesn't matter, but be aware that this will empty the list you point it
	 ** at.
	 **
	 ** 1) https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
	 */
	private List<t> shuffle<t>(List<t> input) {
		List<t> newList = new List<t> ();

		while (input.Count > 0) {
			int i = rng.Next (0, input.Count);

			newList.Add (input [i]);
			input.RemoveAt (i);
		}

		return newList;
	}

	// Return the PRNG object, so other classes can share our PRNG state.
	public Random getRNG() {
		return rng;
	}

	public void setRandomSeed(uint seed) {
		rng = new Random (seed);
	}

	public room[,] getRooms() {
		return gameRooms;
	}

	public List<itemGeneric> getGameItems() {
		return gameItems;
	}

	public List<itemGenericClue> getAllClues() {
		return allClues;
	}

	public List<itemGenericWeapon> getAllWeapons() {
		return allWeapons;
	}

	public List<itemFurniture> getGameFurniture() {
		return gameFurniture;
	}

	public List<itemFurniture> getAllFurniture() {
		return allFurniture;
	}

	public List<character> getGameCharacters() {
		return gameCharacters;
	}

	// Debug functions beyond this point.
	public void printAllCharacters() {
		for(int i = 0; i < gameCharacters.Count; i++)
		{
			character c = gameCharacters [i];
			Debug.Log (string.Format("Character {0}: {1} ({2}) {3} year old {4} {5} with {6} hair and {7} eyes, {8}cm tall. Has {9}.", 
				i, 
				c.getName,
				c.getGender?"male":"female",
				c.getAge,
				c.getNationality,
				c.getRole,
				c.getHair,
				c.getEyes,
				c.getHeight,
				c.getSpecial
			));
		}
	}

	public void printAllFurniture() {
		for (int x = 0; x < gameFurniture.Count; x++)
		{
			itemFurniture f = gameFurniture [x];
			Debug.Log (x + ":::" + f.getItemName + " ::: " + f.getItemDescription + " :::" + f.getFurnitureType);
		}
	}

	public void printAllItems()
	{
		for (int x = 0; x < gameItems.Count; x++)
		{
			itemGeneric item = gameItems [x];
			Debug.Log (x + ":::" + item.getItemName + " ::: " + item.getItemDescription);
		}
	}

	public void printAllRooms()
	{
		//this will print out all the rooms, it will also print out the 5 furniture items in the room, and it will print out the clues and weapons on each peice of furniture
		for (int i = 0; i < (room.x * room.y); i++)
		{
			room r = gameRooms [i / room.x, i % room.x];
			Debug.Log ("Room " + i + " ::: " + r.getName + " ::: " + r.getDescription + " ::: " + r.getRoomType);
			if (r.getCharacterInRoom >= 0)
				Debug.Log ("\tCharacter in room ::: " + gameCharacters[r.getCharacterInRoom].getName);
	
			for (int j = 0; j < room.furnMax; j++) {
				Debug.Log ("\tFurniture " + j + " ::: " + gameFurniture [r.getFurniture [j]].getItemName);
				if (gameFurniture [r.getFurniture [j]].getItem >= 0) {
					itemGeneric it = gameItems [gameFurniture [r.getFurniture [j]].getItem];
					Debug.Log ("\t\tItem " + j + " ::: " + it.getItemName);
				}
			}
		}
	}
}
