﻿using UnityEngine;
using System.Collections;

public class itemFurniture : item
{
    /*Leisure = 1
    Bedding = 2
    Kitchen = 3
    Maintainance = 4
    Study/library = 5
    Dining = 6*/
	room.RoomType roomType;

    //this is the item in/on the furniture.
    //e.g. the KNIFE was found on the RUG
    //the GUN was found on the DESK
    int itemIndex;

    public itemFurniture(string itemNameIn, string itemDescriptionIn, int roomTypeIn) : base(itemNameIn, itemDescriptionIn) {
		roomType = (room.RoomType)roomTypeIn;
		itemIndex = -1;
    }

    public int getFurnitureType
    {
        get
        {
			return (int)roomType;
        }
        set
        {
			roomType = (room.RoomType)value;
        }
    }

    public int getItem
    {
        get
        {
            return itemIndex;
        }
        set
        {
            itemIndex = value;
        }
    }


}
