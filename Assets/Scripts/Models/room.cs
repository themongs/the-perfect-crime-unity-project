﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq.Expressions;

public class room
{
	// Constants to avoid magic numbers.
	public static int x = 4;
	public static int y = 4;
	public static int furnMax = 5;

	public enum Windows { North, South, East, West };
	public enum RoomType : int { Null, Leisure, Bedding, Kitchen, Maintenance, Study, Dining };

    private string name;
    private string description;
    private bool murderRoom;
    private bool northWindow;
    private bool eastWindow;
    private bool southWindow;
    private bool westWindow;

    /*Leisure = 1
    Bedding = 2
    Kitchen = 3
    Maintainance = 4
    Study/library = 5
    Dining = 6
    Allrooms = 7*/
	// We're not currently using Allrooms type.
    RoomType roomType;

    //itemGeneric's are owned by (contained within, or rests on) furniture. 
    //Specifically, an itemGenericWeapon "knife" can be found on top of a itemFurniture "rug"
    //or, an itemGenericClue "note" can be found on top of an itemFurniture "couch"
    //or, an itemGenericWeapon "pen" can be found on an itemFurniture "desk"
    //even the floor is considered furniture and have an itemGeneric on it.

    //furniture has to be located in a room
    //this array of ints refers to the allFurniture index;
    private int[] furnitureIndex;

    //characters are located in rooms;
    //this array of ints refers to the allFurniture index;
    private int characterIndex;

    public room(string nameIn, string descriptionIn, bool windowNorth, bool windowEast, bool windowSouth, bool windowWest, int roomTypeIn)
    {
        name = nameIn;
        description = descriptionIn;

        northWindow = windowNorth;
        eastWindow = windowEast;
        southWindow = windowSouth;
        westWindow = windowWest;
        murderRoom = false;
		roomType = (RoomType)roomTypeIn;

        //default unassigned character
        characterIndex = -1;

        //furniture not assigned yet
		furnitureIndex = new int[]{ -1, -1, -1, -1, -1 };
        
    }

	public static int getFurnTypeCount
	{
		get {
			return Enum.GetNames (typeof(RoomType)).Length;
		}
	}

    public string getName
    {
        get
        {
            return name;
        }
    }
    public string getDescription
    {
        get
        {
            return description;
        }
    }
    public bool getMurderRoom
    {
        get
        {
            return murderRoom;
        }
        set
        {
            murderRoom = value;
        }
    }
	public room.RoomType getRoomType
    {
        get
        {
            return roomType;
        }
        set
        {
            roomType = value;
        }
    }
    public int[] getFurniture
    {
        get
        {
            return furnitureIndex;
        }
        set
        {
			Array.Copy(value, furnitureIndex, furnMax);
        }
    }
    public int getCharacterInRoom
    {
        get
        {
            return characterIndex;
;
        }
        set
        {
            characterIndex = value;
        }
    }

	public bool hasWindow (Windows windows) {
		switch (windows) {
		case Windows.North:
			return northWindow;
		case Windows.East:
			return eastWindow;
		case Windows.South:
			return southWindow;
		case Windows.West:
			return westWindow;
		default:
			throw new Exception ("Invalid Window requested - this shouldn't happen.");
		}
	}
}
