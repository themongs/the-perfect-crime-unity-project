﻿using UnityEngine;
using System.Collections;

public class itemGenericWeapon : itemGeneric
{
    protected bool isMurderWeapon;
    protected string action;

    public itemGenericWeapon(string itemNameIn, string itemDescriptionIn, string actionIn) : base(itemNameIn, itemDescriptionIn) {
        action = actionIn;
        isMurderWeapon = false;
    }

    public bool murderWeapon
    {
        get
        {
            return isMurderWeapon;
        }
        set
        {
            isMurderWeapon = value;
        }
    }
    public string getAction
    {
        get
        {
            return action;
        }
    }
}
