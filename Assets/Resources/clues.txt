currency*{3} currency.
letter*a letter of demand from {1} stating that someone with the initials {5}. {6}. owes them $5000 in unpaid loans.
handkerchief*a handkerchief embroidered with the initials {5}. {6}.
pen*an expensive looking tortoise shell Parker pen with the initials {5}. {6}.
business card*an old business card soliciting a {2}'s services. The name is all smudged and cannot be discerned.
leather gloves*high quaility deer skin gloves with the initials {5}. {6}. embroided on the wrist.
pocket watch*a pocket watch with the name {9} engraved on the back.
locket*an intricately designed silver locket with a picture of someone's lover inside of it. On further inspection it appears the {4} pictured is named {10} {6}.
diary*a moleskin diary with the name {9} inscribed on the first page.
book*a book detailing the technical requirements of being a {2}.
footprint*a dirty footprint, it looks like a person of {12} stature left it.
