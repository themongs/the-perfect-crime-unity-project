﻿using NSubstitute;
using NUnit.Framework;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace PerfectCrime
{
	public class GameTests
	{
		[Test]
		public void testLoad16Rooms ()
		{
			game g = new game();

			string data = "a*1*a\nb*5*a\nc*3*a\nd*2*a\ne*4*a\na*1*a\na*1*a\na*1*a\n" +
						  "a*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);

			g.LoadRooms (reader);
			room[,] rooms = g.getRooms ();
			Assert.AreEqual ("a", rooms[0, 0].getName);
			Assert.AreEqual ("b", rooms [0, 1].getName);
		}

		[Test]
		public void testLoad5Rooms ()
		{
			game g = new game ();

			string data = "a*1*a\nb*5*a\nc*3*a\nd*2*a\ne*4*x\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			g.LoadRooms (reader);
			room[,] rooms = g.getRooms ();
			Assert.AreEqual ("a", rooms [0, 0].getName);
			Assert.AreEqual ("b", rooms [0, 1].getName);
			Assert.AreEqual ("c", rooms [0, 2].getName);
			Assert.AreEqual ("d", rooms [0, 3].getName);
			Assert.AreEqual ("e", rooms [1, 0].getName);

			// Should repeat from here
			Assert.AreEqual ("a", rooms [1, 1].getName);
			Assert.AreEqual ("b", rooms [1, 2].getName);
			Assert.AreEqual ("c", rooms [1, 3].getName);
			Assert.AreEqual ("d", rooms [2, 0].getName);
			Assert.AreEqual ("e", rooms [2, 1].getName);

			Assert.AreEqual ("a", rooms [2, 2].getName);
			Assert.AreEqual ("b", rooms [2, 3].getName);
			Assert.AreEqual ("c", rooms [3, 0].getName);
			Assert.AreEqual ("d", rooms [3, 1].getName);
			Assert.AreEqual ("e", rooms [3, 2].getName);

			Assert.AreEqual ("a", rooms [3, 3].getName);
		}

		[Test]
		public void testLoadWeaponsShuffle ()
		{
			game g = new game ();

			string data = "knife*stabbing*A knife.\n" +
			              "spoon*stabbing*Not as good as a knife.\n" +
			              "gun*shooting*Long distance stabbing.";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			g.setRandomSeed (2);
			g.LoadWeapons (reader);

			List<itemGenericWeapon> items = g.getAllWeapons ();
			Assert.AreEqual ("knife", items [0].getItemName);
			Assert.AreEqual ("spoon", items [1].getItemName);
			Assert.AreEqual ("gun", items [2].getItemName);

			ms.Seek (0, 0);
			g = new game ();
			g.setRandomSeed (3);
			g.LoadWeapons (reader);

			items = g.getAllWeapons ();
			Assert.AreEqual ("spoon", items [0].getItemName);
			Assert.AreEqual ("gun", items [1].getItemName);
			Assert.AreEqual ("knife", items [2].getItemName);
		}

		[Test]
		public void testLoadClues()
		{
			game g = new game ();

			string data = "Clue01*EMPTY\nClue02*EMPTY\nClue03*EMPTY\nClue04*EMPTY\nClue05*EMPTY";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			string[] expected = {"Clue01", "Clue03", "Clue02", "Clue05", "Clue04"};
			g.setRandomSeed (2);
			g.LoadClues (reader);

			List<itemGenericClue> items = g.getAllClues ();
			Assert.AreEqual (expected.Length, items.Count);
			for (int i = 0; i < expected.Length; i++)
				Assert.AreEqual (expected[i], items [i].getItemName);

			ms.Seek (0, 0);
			string[] expected2 = {"Clue04", "Clue03", "Clue05", "Clue02", "Clue01"};
			g = new game ();
			g.setRandomSeed (3);
			g.LoadClues (reader);

			items = g.getAllClues ();
			Assert.AreEqual (expected.Length, items.Count);
			for (int i = 0; i < expected2.Length; i++)
				Assert.AreEqual (expected2[i], items [i].getItemName);
		}

		[Test]
		public void testLoadFurnitureMissingRoom ()
		{
			game g = new game ();

			string data = "bed*5*a\n" +
						  "bath*2*b\n" +
						  "beyond*3*c\n" +
						  "joke*4*d\n" +
						  "went*5*t\n" +
						  "too*6*a\n" +
						  "far*3*a\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			Assert.Throws(typeof(System.Exception), delegate{ g.LoadFurniture (reader); });
		}

		[Test]
		public void testLoadFurnitureMissingRoomAgain ()
		{
			// Test the high-end as well, as I spotted a place where I thought there might be
			// a bug. Turns out it wasn't, but might as well leave the test in.
			game g = new game ();

			string data = "bed*1*a\n" +
				"bath*2*b\n" +
				"beyond*3*c\n" +
				"joke*4*d\n" +
				"went*5*t\n" +
				"too*2*a\n" +
				"far*3*a\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			Assert.Throws(typeof(System.Exception), delegate{ g.LoadFurniture (reader); });
		}

		[Test]
		public void testLoadFurnitureIndexOOB ()
		{
			game g = new game ();

			string data = "bed*7*a\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);
			Assert.Throws(typeof(System.Exception), delegate{ g.LoadFurniture (reader); });
		}

		[Test]
		public void testLoadFurnitureShuffle ()
		{
			game g = new game ();

			string data = "bed*1*a\n" +
			              "bath*2*b\n" +
			              "beyond*3*c\n" +
			              "joke*4*d\n" +
			              "went*5*t\n" +
			              "too*6*a\n" +
			              "far*3*a\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			g.setRandomSeed (1);
			g.LoadFurniture (reader);

			List<itemFurniture> items = g.getAllFurniture ();
			Assert.AreEqual ("far", items [0].getItemName);
			Assert.AreEqual ("too", items [1].getItemName);
			Assert.AreEqual ("beyond", items [2].getItemName);
			Assert.AreEqual ("went", items [3].getItemName);
			Assert.AreEqual ("joke", items [4].getItemName);
			Assert.AreEqual ("bath", items [5].getItemName);
			Assert.AreEqual ("bed", items [6].getItemName);

			ms.Seek (0, 0);
			g = new game ();
			g.setRandomSeed (5);
			g.LoadFurniture (reader);

			items = g.getAllFurniture ();
			Assert.AreEqual ("bed", items [0].getItemName);
			Assert.AreEqual ("bath", items [1].getItemName);
			Assert.AreEqual ("far", items [2].getItemName);
			Assert.AreEqual ("too", items [3].getItemName);
			Assert.AreEqual ("beyond", items [4].getItemName);
			Assert.AreEqual ("went", items [5].getItemName);
			Assert.AreEqual ("joke", items [6].getItemName);
		}

		[Test]
		public void testLoadCharacters ()
		{
			game g = new game ();
			string[] expected = { "fourandahalf", "cameron", "barton", "two", "lincoln",
				"glenn", "one", "three", "five", "james", "four" };

			g.setRandomSeed (1);
			g.LoadCharacters (getValidCharacterData ());

			List<character> chars = g.getGameCharacters();
			Assert.AreEqual (expected.Length, chars.Count);
			for (int i = 0; i < expected.Length; i++)
				Assert.AreEqual (expected[i], chars [i].getName);

			// Check guilty flags - only first char should be guilty!
			Assert.AreEqual (true, chars [0].getGuilty);
			for (int i = 1; i < expected.Length; i++)
				Assert.AreEqual (false, chars [i].getGuilty);

			// only last char should be vitim
			for (int i = 0; i < expected.Length - 1; i++)
				Assert.AreEqual (false, chars [i].getVictim);
			Assert.AreEqual (true, chars [expected.Length - 1].getVictim);

			// Restart the stream and try again with a different random seed.
			g = new game ();
			g.setRandomSeed (131166);
			g.LoadCharacters (getValidCharacterData ());

			string[] expected2 = { "one", "barton", "three", "fourandahalf", "james", 
				"five", "cameron", "glenn", "lincoln", "two", "four"};

			chars = g.getGameCharacters();
			Assert.AreEqual (expected2.Length, chars.Count);
			for (int i = 0; i < expected2.Length; i++)
				Assert.AreEqual (expected2[i], chars [i].getName);

			// Check guilty flags - only first char should be guilty!
			Assert.AreEqual (true, chars [0].getGuilty);
			for (int i = 1; i < expected2.Length; i++)
				Assert.AreEqual (false, chars [i].getGuilty);

			// only last char should be vitim
			for (int i = 0; i < expected2.Length - 1; i++)
				Assert.AreEqual (false, chars [i].getVictim);
			Assert.AreEqual (true, chars [expected2.Length - 1].getVictim);
		}

		[Test]
		public void testLoadCharactersTooFew ()
		{
			game g = new game ();
			string data = "james*m\n" +
				"glenn*m\n" +
				"lincoln*m\n" +
				"barton*m\n" +
				"cameron*m\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			g.setRandomSeed (1);

			// If we're going to have 8 character buttons to accuse other characters
			// the game needs to scream loudly if it doesn't have at least 10 characters
			Assert.Throws(typeof(System.Exception), delegate{ g.LoadCharacters(reader); });
		}

		[Test]
		public void testLoadCharactersTooMany ()
		{
			List<character> chars;
			game g = new game ();
			string data = "james*m\n" +
				"glenn*m\n" +
				"lincoln*m\n" +
				"barton*m\n" +
				"cameron*m\n" +
				"one*f\n" +
				"two*m\n" +
				"three*f\n" +
				"four*m\n" +
				"five*f\n" +
				"six*m\n" +
                "sixpointfive*m\n" +
                "seven*f\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			g.setRandomSeed (1);
			g.LoadCharacters (reader);

			// Since there are more than 11 characters in the file, the game needs to
			// load them all, shuffle the list, then prune it back to 11 before 
			// setting up the game conditions.
			chars = g.getGameCharacters ();
			Assert.AreEqual (11, chars.Count);
		}

		[Test]
		public void testLoadCharactersTwice ()
		{
			game g = new game ();
			g.setRandomSeed (1);
			g.LoadCharacters (getValidCharacterData ());

			// LoadCharacters() being run twice *should* complain
			Assert.Throws (typeof(System.Exception), delegate { g.LoadCharacters (getValidCharacterData ()); });
		}

		// This monster test case checks that the game starts up in a sane,
		// known-good fashion. It could probably be broken up into multiple
		// tests if I knew how NUnit's setup/teardown features worked.
		// ATM if you want to know what broke, you have to compare this test
		// to the line number in the stack trace. 
		[Test]
		public void testGameState ()
		{
			game g = new game ();

			g.setRandomSeed (8675309);
			g.LoadCharacters (getValidCharacterData ());
			g.LoadRooms (getValidRoomData());

			// Running SetupFurniture without any furniture should throw an exception.
			Assert.Throws(typeof(System.Exception), delegate{ g.SetupFurniture(); });
			g.LoadFurniture (getValidFurnitureData ());

			// Running SetupMurder without any weapons should throw an exception.
			Assert.Throws(typeof(System.Exception), delegate{ g.SetupMurder(); });
			g.LoadWeapons (getValidWeaponData ());

			// Running SetupMurder without any furniture in the room should throw an exception.
			Assert.Throws(typeof(System.Exception), delegate{ g.SetupMurder(); });

			// Check there's no furniture in any room yet.
			foreach (room r in g.getRooms()) {
				for (int i = 0; i < room.furnMax; i++)
					Assert.AreEqual (-1, r.getFurniture [i]);
			}

			// Now check SetupFurniture is working correctly.
			g.SetupFurniture ();
			int checksum = 0;
			foreach (room r in g.getRooms()) {
				for (int i = 0; i < room.furnMax; i++) {
					Assert.AreNotEqual (-1, r.getFurniture [i]);
					checksum = checksum + r.getFurniture [i];
				}
			}
			Assert.AreEqual (3160, checksum);

			// Running SetupWitnesses without a murder should throw an exception.
			Assert.Throws(typeof(System.Exception), delegate{ g.SetupWitnesses(); });

			// This should run without exception now.
			g.SetupMurder();
			g.SetupWitnesses();

			// Check the murder details are correct.
			Assert.AreEqual("knife",g.getMurderWeapon.getItemName);
			Assert.AreEqual("bed",g.getHidingPlace.getItemName);
			Assert.AreEqual ("d", g.getMurderRoom.getName);

			// Room #4 (0,3) should be the murder room in this scenario.
			room[,] rooms = g.getRooms();
            Assert.AreSame(g.getMurderRoom, rooms[0,3]);
			Assert.AreEqual (10, rooms [0,3].getCharacterInRoom);

			// Check the witnesses are in the right place.
			Assert.AreEqual (1, rooms [0,0].getCharacterInRoom);
			Assert.AreEqual (2, rooms [0,1].getCharacterInRoom);
			Assert.AreEqual (3, rooms [0,2].getCharacterInRoom);
			Assert.AreEqual (4, rooms [1,0].getCharacterInRoom);
			Assert.AreEqual (5, rooms [1,1].getCharacterInRoom);
			Assert.AreEqual (6, rooms [1,2].getCharacterInRoom);

			// Character 0 is the murderer and should not be assigned a room
			// all other rooms should be empty.
			for (int i = 11; i < (room.x * room.y); i++) {
				Assert.AreEqual (-1, rooms [i/room.x, i%room.x].getCharacterInRoom);
			}

			// Check that all the other pieces of furniture are empty now
			itemFurniture hiding = g.getHidingPlace;
			int found = 0;
			for (int i=0; i < (room.x * room.y * room.furnMax); i++) {
				itemFurniture itm = g.getFurnitureAt(i);

				if (hiding == itm)
					found++;
				else if (itm.getItem != -1)
					Assert.AreEqual(-1, itm.getItem);
			}
			Assert.AreEqual (1, found);

			// SetupItems with out clues should throw an exception.
			Assert.Throws(typeof(System.Exception), delegate{ g.SetupItems(); });

			g.LoadClues (getValidClueData ());

			// Now setup all the other items, and make sure there's no empty furniture
			g.SetupItems();
			found = 0;
			checksum = 0;
			for (int i=0; i < (room.x * room.y * room.furnMax); i++) {
				itemFurniture itm = g.getFurnitureAt(i);
				if (itm.getItem != -1)
					found++;
				checksum = checksum + itm.getItem;
			}
			Assert.AreEqual ((room.x * room.y * room.furnMax), found);
			// A lazy way of ensuring that everything isn't the murder weapon or something
			// equally silly.
			Assert.AreEqual (3160, checksum);
		}

		[Test]
		public void testClobberWeapon ()
		{
			game g = new game ();

			g.setRandomSeed (1);
			g.LoadCharacters (getValidCharacterData ());
			g.LoadRooms (getValidRoomData());
			g.LoadFurniture (getValidFurnitureData ());
			g.LoadWeapons (getValidWeaponData ());
			g.LoadClues (getValidClueData ());

			g.SetupFurniture ();
			g.SetupMurder();
			g.SetupItems();

			Assert.DoesNotThrow(delegate{ Assert.AreNotSame(-1, g.getHidingPlace); });
		}

		[Test]
		public void testGetCharacterFunctions ()
		{
			game g = new game ();

			g.setRandomSeed (1);
			g.LoadCharacters (getValidCharacterData ());

			List<character> chars = g.getGameCharacters();

			Assert.AreSame (chars [0], g.getPlayerCharacter);
			Assert.AreSame (chars [chars.Count - 1], g.getVictimCharacter);
			Assert.AreSame (chars [2], g.getCharacterAt (2));
		}

		// These functions return valid data for testing the behavior of functions
		// where you don't care enough to make your own data.
		public static StreamReader getValidRoomData()
		{
			string data = "a*1*a\nb*5*a\nc*3*a\nd*2*a\ne*4*a\na*1*a\na*1*a\na*1*a\n" +
				"a*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);

			return reader;
		}

		public static StreamReader getValidFurnitureData()
		{
			string data = "couch*1*a\n" +
				"bed*2*b\n" +
				"sink*3*c\n" +
				"workbench*4*d\n" +
				"bookshelf*5*t\n" +
				"table*6*a\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			return reader;
		}

		public static StreamReader getValidWeaponData()
		{
			string data = "knife*stabbing*A knife.\n" +
				"spoon*stabbing*Not as good as a knife.\n" +
				"gun*shooting*Long distance stabbing.";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			return reader;
		}

		public static StreamReader getValidCharacterData()
		{
			string data = "james*m\n" +
				"glenn*m\n" +
				"lincoln*m\n" +
				"barton*m\n" +
				"cameron*m\n" +
				"one*f\n" +
				"two*m\n" +
				"three*f\n" +
				"four*m\n" +
                "fourandahalf*m\n" +
                "five*f\n";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			return reader;
		}

		public static StreamReader getValidClueData()
		{
			string data = "Clue01*EMPTY\nClue02*EMPTY\nClue03*EMPTY\nClue04*EMPTY\nClue05*EMPTY";
			MemoryStream ms = new MemoryStream (System.Text.Encoding.Default.GetBytes (data));
			StreamReader reader = new StreamReader (ms, true);

			return reader;
		}
	}
}