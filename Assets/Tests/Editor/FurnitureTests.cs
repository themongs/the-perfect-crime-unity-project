﻿using NSubstitute;
using NUnit.Framework;

namespace PerfectCrime
{
	public class FurnitureModelTests
	{
		[Test]
		public void testFurnitureConstructor ()
		{
			itemFurniture f = new itemFurniture ("Couch", "A couch. It looks comfy!", 1);

			Assert.AreEqual ("Couch", f.getItemName);
			Assert.AreEqual ("A couch. It looks comfy!", f.getItemDescription);
			Assert.AreEqual (1, f.getFurnitureType);
		}
	}

}