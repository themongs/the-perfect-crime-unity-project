﻿using NSubstitute;
using NUnit.Framework;

namespace PerfectCrime
{
	public class RoomModelTests
	{
		[Test]
		public void testRoomConstructor ()
		{
			room r = new room("Spare Bedroom","A cosy bedroom",false,false,false,false,2);
			Assert.AreEqual ("Spare Bedroom", r.getName);
			Assert.AreEqual ("A cosy bedroom", r.getDescription);
			Assert.AreEqual (2, (int)r.getRoomType);
		}

		[Test]
		public void testMurderRoom ()
		{
			room r = new room("Spare Bedroom","A cosy bedroom",false,false,false,false,2);
			Assert.AreEqual (false, r.getMurderRoom);

			// Now set it to true and make sure it sticks.
			r.getMurderRoom = true;
			Assert.AreEqual (true, r.getMurderRoom);
		}

		[Test]
		public void testWindows ()
		{
			room r = new room("Spare Bedroom","A cosy bedroom",true,false,true,false,2);

			Assert.AreEqual(true, r.hasWindow(room.Windows.North));
			Assert.AreEqual(false, r.hasWindow(room.Windows.East));
			Assert.AreEqual(true, r.hasWindow(room.Windows.South));
			Assert.AreEqual(false, r.hasWindow(room.Windows.West));

			// Try a different configuration
			r = new room("Spare Bedroom","A cosy bedroom",false,true,false,true,2);

			Assert.AreEqual(false, r.hasWindow(room.Windows.North));
			Assert.AreEqual(true, r.hasWindow(room.Windows.East));
			Assert.AreEqual(false, r.hasWindow(room.Windows.South));
			Assert.AreEqual(true, r.hasWindow(room.Windows.West));
		}
	}

}
