﻿using NSubstitute;
using NUnit.Framework;

namespace PerfectCrime
{
	public class CharacterModelTests
	{
		[Test]
		public void testCharacterConstructor ()
		{
			character c = new character ("Test",'m',"Tester","Blonde","Blue","Australian",51,180, "glasses");
			Assert.AreEqual ("Test", c.getName);
			Assert.AreEqual ("Tester", c.getRole);
			Assert.AreEqual (51, c.getAge);
			Assert.AreEqual ("Blonde", c.getHair);
			Assert.AreEqual ("Blue", c.getEyes);
			Assert.AreEqual ("Australian", c.getNationality);
			Assert.AreEqual (180, c.getHeight);
			// TODO: Possibly rename getGender to isMale or something, or make it return a string instead of bool?
			Assert.AreEqual (true, c.getGender);

			// Try a different character with different settings.
			c = new character ("Juanita",'f',"Company CEO","Dark","Brown","Spanish",33,155,"wheelchair");
			Assert.AreEqual ("Juanita", c.getName);
			Assert.AreEqual ("Company CEO", c.getRole);
			Assert.AreEqual (33, c.getAge);
			Assert.AreEqual ("Dark", c.getHair);
			Assert.AreEqual ("Brown", c.getEyes);
			Assert.AreEqual ("Spanish", c.getNationality);
			Assert.AreEqual (155, c.getHeight);
			Assert.AreEqual (false, c.getGender);
		}

		[Test]
		public void testCharacterGuilty ()
		{
			character c = new character ("", 'm', "", "", "", "", 0, 0, "");

			Assert.AreEqual (false, c.getGuilty);

			// Now set it to guilty and verify that 
			c.getGuilty = true;
			Assert.AreEqual (true, c.getGuilty);
		}

		[Test]
		public void testCharacterVictim ()
		{
			character c = new character ("", 'm', "", "", "", "", 0, 0, "");

			Assert.AreEqual (false, c.getVictim);

			c.getVictim = true;
			Assert.AreEqual (true, c.getVictim);
		}

		[Test]
		public void testCharacterSubjectiveHeight ()
		{
			character s = new character ("", 'm', "", "", "", "", 0, 159, "");
			character sa = new character ("", 'm', "", "", "", "", 0, 160, "");
			character ta = new character ("", 'm', "", "", "", "", 0, 169, "");
			character t = new character ("", 'm', "", "", "", "", 0, 170, "");

			Assert.AreEqual ("short", s.getSubjectiveHeight);
			Assert.AreEqual ("average", sa.getSubjectiveHeight);
			Assert.AreEqual ("average", ta.getSubjectiveHeight);
			Assert.AreEqual ("tall", t.getSubjectiveHeight);
		}

		[Test]
		public void testCharacterPronouns ()
		{
			character m = new character ("", 'm', "", "", "", "", 0, 159, "");
			character f = new character ("", 'f', "", "", "", "", 0, 160, "");

			Assert.AreEqual ("him", m.getPronounObjective());
			Assert.AreEqual ("her", f.getPronounObjective());
			Assert.AreEqual ("He", m.getPronounSubjective());
			Assert.AreEqual ("She", f.getPronounSubjective());
			Assert.AreEqual ("his", m.getPronounPossessive());
			Assert.AreEqual ("her", f.getPronounPossessive());
		}

		[Test]
		public void testCharacterFirstLastNames()
		{
			character one = new character ("Test Character",'m',"","","","",0,150, "");
			character two = new character ("Another Test", 'm', "", "", "", "", 0, 160, "");

			Assert.AreEqual ("Test", one.getFirstName);
			Assert.AreEqual ("Character", one.getLastName);
			Assert.AreEqual ("Another", two.getFirstName);
			Assert.AreEqual ("Test", two.getLastName);
		}
	}

}
