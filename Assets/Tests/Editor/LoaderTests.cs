﻿using NSubstitute;
using NUnit.Framework;
using System.IO;

namespace PerfectCrime
{
	public class LoaderTests
	{
		[Test]
		public void testRoomLoader ()
		{
			string data = "Master Bedroom*2*The master suite\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			RoomLoader l = new RoomLoader (reader, rng);
			room r = l.Next ();

			Assert.AreEqual ("Master Bedroom", r.getName);
			Assert.AreEqual (2, (int)r.getRoomType);
			Assert.AreEqual ("The master suite", r.getDescription);
		}

		[Test]
		public void testRoomLoaderEOF ()
		{
			string data = "Master Bedroom*2*The master suite\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			RoomLoader l = new RoomLoader (reader, rng);

			Assert.DoesNotThrow(delegate  { l.Next ();});
			Assert.Throws(typeof(EndOfStreamException), delegate { l.Next ();});
		}

		[Test]
		public void testRoomLoaderMalformed ()
		{
			string data = "Master Bedroom*2\nMaster Bedroom*2*The master suite*\n" +
				"a*a*a\na*7*a\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			RoomLoader l = new RoomLoader (reader, rng);

			// First line is only two elements long, second is four.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});

			// Third line has a letter where integer should be
			Assert.Throws(typeof(System.Exception), delegate { l.Next ();});

			// Fourth line has an out-of-range type indicator.
			Assert.Throws(typeof(System.Exception), delegate { l.Next ();});
		}

		[Test]
		public void testRoomLoaderWindows ()
		{
			// Sixteen empty rooms.
			string data = "a*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\n" +
				"a*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\na*1*a\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			RoomLoader l = new RoomLoader (reader, rng);

			// Room array as we'd use in consuming code.
			room[,] allRooms = new room[room.y, room.x];
			int roomCount = 0;

			while (true) {
				try {
					allRooms[roomCount / room.x, roomCount % room.x] = l.Next();
					roomCount++;
				}
				catch (EndOfStreamException) {
					break;
				}
			}

			// Don't test every window, just a few because I'm lazy.

			// A middle room, chosen by fair dice roll, guaranteed to be random
			Assert.AreEqual(false, allRooms[1,2].hasWindow(room.Windows.North));
			Assert.AreEqual(false, allRooms[1,2].hasWindow(room.Windows.East));
			Assert.AreEqual(false, allRooms[1,2].hasWindow(room.Windows.South));
			Assert.AreEqual(false, allRooms[1,2].hasWindow(room.Windows.West));

			// North west room.
			Assert.AreEqual(true, allRooms[0,0].hasWindow(room.Windows.North));
			Assert.AreEqual(false, allRooms[0,0].hasWindow(room.Windows.East));
			Assert.AreEqual(false, allRooms[0,0].hasWindow(room.Windows.South));
			Assert.AreEqual(true, allRooms[0,0].hasWindow(room.Windows.West));

			// North east room.
			Assert.AreEqual(true, allRooms[0,room.x -1].hasWindow(room.Windows.North));
			Assert.AreEqual(true, allRooms[0,room.x -1].hasWindow(room.Windows.East));
			Assert.AreEqual(false, allRooms[0,room.x -1].hasWindow(room.Windows.South));
			Assert.AreEqual(false, allRooms[0,room.x -1].hasWindow(room.Windows.West));

			// South west room.
			Assert.AreEqual(false, allRooms[room.y -1,0].hasWindow(room.Windows.North));
			Assert.AreEqual(false, allRooms[room.y -1,0].hasWindow(room.Windows.East));
			Assert.AreEqual(true, allRooms[room.y -1,0].hasWindow(room.Windows.South));
			Assert.AreEqual(true, allRooms[room.y -1,0].hasWindow(room.Windows.West));

			// South east room.
			Assert.AreEqual(false, allRooms[room.y -1,room.x -1].hasWindow(room.Windows.North));
			Assert.AreEqual(true, allRooms[room.y -1,room.x -1].hasWindow(room.Windows.East));
			Assert.AreEqual(true, allRooms[room.y -1,room.x -1].hasWindow(room.Windows.South));
			Assert.AreEqual(false, allRooms[room.y -1,room.x -1].hasWindow(room.Windows.West));
		}

		// Test ClueLoader class
		[Test]
		public void testClueLoader ()
		{
			string data = "Footprint*A size 13 clown shoe.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			ClueLoader l = new ClueLoader (reader, rng);
			itemGenericClue clue = l.Next ();

			Assert.AreEqual ("Footprint", clue.getItemName);
			Assert.AreEqual ("A size 13 clown shoe.", clue.getItemDescription);
		}

		[Test]
		public void testClueLoaderEOF ()
		{
			string data = "Footprint*A size 13 clown shoe.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			ClueLoader l = new ClueLoader (reader, rng);

			Assert.DoesNotThrow(delegate  { l.Next ();});
			Assert.Throws(typeof(EndOfStreamException), delegate { l.Next ();});
		}

		[Test]
		public void testClueLoaderMalformed ()
		{
			string data = "FootprintA size 13 clown shoe.\nFootprint*A size 13 clown shoe.*Hello\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			ClueLoader l = new ClueLoader (reader, rng);

			// First line is only one element long, second is three.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
		}

		// Test WeaponLoader class
		[Test]
		public void testWeaponLoader ()
		{
			string data = "knife*stabbing*A bowie hunting knife. 17\" long, razor sharp on one side.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			WeaponLoader l = new WeaponLoader (reader, rng);
			itemGenericWeapon w = l.Next ();

			Assert.AreEqual ("knife", w.getItemName);
			Assert.AreEqual ("A bowie hunting knife. 17\" long, razor sharp on one side.", w.getItemDescription);
			Assert.AreEqual ("stabbing", w.getAction);
		}

		[Test]
		public void testWeaponLoaderEOF ()
		{
			string data = "knife*stabbing*A bowie hunting knife. 17\" long, razor sharp on one side.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			WeaponLoader l = new WeaponLoader (reader, rng);

			Assert.DoesNotThrow(delegate  { l.Next ();});
			Assert.Throws(typeof(EndOfStreamException), delegate { l.Next ();});
		}

		[Test]
		public void testWeaponLoaderMalformed ()
		{
			string data = "knife*stabbingA bowie hunting knife. 17\" long, razor sharp on one side.\n" +
				"knife*stabbing*A bowie hunting knife. 17\" long, razor sharp on one side. *Note: It stabs.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			WeaponLoader l = new WeaponLoader (reader, rng);

			// First line is only two elements long, second is four.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
		}

		// Test FurnitureLoader class
		[Test]
		public void testFurnitureLoader ()
		{
			string data = "armoire*2*An old, musty-smelling armoire.\nbar stool*3*Four legs and a padded seat.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			FurnitureLoader l = new FurnitureLoader (reader, rng);
			itemFurniture f = l.Next ();

			Assert.AreEqual ("armoire", f.getItemName);
			Assert.AreEqual ("An old, musty-smelling armoire.", f.getItemDescription);
			Assert.AreEqual (2, f.getFurnitureType);

			// Check second item.
			f = l.Next ();

			Assert.AreEqual ("bar stool", f.getItemName);
			Assert.AreEqual ("Four legs and a padded seat.", f.getItemDescription);
			Assert.AreEqual (3, f.getFurnitureType);
		}

		[Test]
		public void testFurnitureLoaderEOF ()
		{
			string data = "armoire*2*An old, musty-smelling armoire.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			FurnitureLoader l = new FurnitureLoader (reader, rng);

			Assert.DoesNotThrow(delegate  { l.Next ();});
			Assert.Throws(typeof(EndOfStreamException), delegate { l.Next ();});
		}

		[Test]
		public void testFurnitureLoaderMalformed ()
		{
			string data = "armoire2*An old, musty-smelling armoire.\nbar stool*3*Four legs and a padded seat.*\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			FurnitureLoader l = new FurnitureLoader (reader, rng);

			// First line is only two elements long, second is four.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
		}

		[Test]
		public void testFurnitureLoaderIndexOOB ()
		{
			string data = "armoire*7*An old, musty-smelling armoire.\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);
			Random rng = new Random (1);

			FurnitureLoader l = new FurnitureLoader (reader, rng);

			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
		}

		// Test CharacterLoader class
		[Test]
		public void testCharacterLoader ()
		{
			string data = "Mad Max*m\nTina Turner*f\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);

			CharacterLoader l = new CharacterLoader (reader, new Random());
			// NOTE: This test will fail in Unity itself because Unity and Mono-develop use different
			// PRNGs. This is not necessarily considered a failure.
			l.setRandomSeed(8675309);
			character c = l.Next ();

			Assert.AreEqual ("Mad Max", c.getName);
			Assert.AreEqual (true, c.getGender);
			Assert.AreEqual ("blonde", c.getHair);
			Assert.AreEqual ("green", c.getEyes);
			Assert.AreEqual ("Plumber", c.getRole);
			Assert.AreEqual ("Russian", c.getNationality);
			Assert.AreEqual (21, c.getAge);
			Assert.AreEqual (166, c.getHeight);

			// Check second character, but with same seed.
			l.setRandomSeed(8675309);
			c = l.Next ();

			Assert.AreEqual ("Tina Turner", c.getName);
			Assert.AreEqual (false, c.getGender);
			Assert.AreEqual ("blonde", c.getHair);
			Assert.AreEqual ("green", c.getEyes);
			Assert.AreEqual ("Plumber", c.getRole);
			Assert.AreEqual ("Russian", c.getNationality);
			Assert.AreEqual (21, c.getAge);
			Assert.AreEqual (166, c.getHeight);

			// Return to start of "file", repeat with a different seed.
			ms.Seek (0, 0);

			l = new CharacterLoader (reader, new Random());
			l.setRandomSeed(131166);
			c = l.Next ();

			Assert.AreEqual ("Mad Max", c.getName);
			Assert.AreEqual (true, c.getGender);
			Assert.AreEqual ("brown", c.getHair);
			Assert.AreEqual ("blue", c.getEyes);
			Assert.AreEqual ("Computer Analyst", c.getRole);
			Assert.AreEqual ("English", c.getNationality);
			Assert.AreEqual (40, c.getAge);
			Assert.AreEqual (175, c.getHeight);

			// Check second character, but without resetting seed.
			c = l.Next ();

			Assert.AreEqual ("Tina Turner", c.getName);
			Assert.AreEqual (false, c.getGender);
			Assert.AreEqual ("blonde", c.getHair);
			Assert.AreEqual ("hazel", c.getEyes);
			Assert.AreEqual ("Scientist", c.getRole);
			Assert.AreEqual ("Chinese", c.getNationality);
			Assert.AreEqual (40, c.getAge);
			Assert.AreEqual (152, c.getHeight);
		}

		[Test]
		public void testCharacterLoaderEOF ()
		{
			string data = "Mad Max*m\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);

			CharacterLoader l = new CharacterLoader (reader, new Random());

			Assert.DoesNotThrow(delegate  { l.Next ();});
			Assert.Throws(typeof(EndOfStreamException), delegate { l.Next ();});
		}

		[Test]
		public void testCharacterLoaderMalformed ()
		{
			string data = "Mad Maxm\nMad Max*m*A bit of a badass.\nMad Max*a\n";
			MemoryStream ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(data));
			StreamReader reader = new StreamReader (ms, true);

			CharacterLoader l = new CharacterLoader (reader, new Random());

			// First line is only one element long, second is three.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
			// Third line has a gender field that's neither m nor f.
			Assert.Throws(typeof(System.Exception), delegate {l.Next ();});
		}
	}
}