﻿using NSubstitute;
using NUnit.Framework;

namespace PerfectCrime
{
	public class ItemGenericModelTests
	{
		[Test]
		public void testGenericWeapon ()
		{
			itemGenericWeapon w = new itemGenericWeapon ("Gun", "It makes a loud noise", "shoot");
			itemGeneric g = w;

			Assert.AreEqual ("Gun", w.getItemName);
			Assert.AreEqual ("It makes a loud noise", w.getItemDescription);
			Assert.AreEqual ("shoot", w.getAction);

			// Test Generic functions
			Assert.AreEqual ("Gun", g.getItemName);
			Assert.AreEqual ("It makes a loud noise", g.getItemDescription);

			// Test setting murder weapon
			Assert.AreEqual(false, w.murderWeapon);
			w.murderWeapon = true;
			Assert.AreEqual (true, w.murderWeapon);
		}

		[Test]
		public void testGenericClue ()
		{
			itemGenericClue c = new itemGenericClue("Footprint","Size 13 clown shoe");
			itemGeneric g = c;

			Assert.AreEqual ("Footprint", c.getItemName);
			Assert.AreEqual ("Size 13 clown shoe", c.getItemDescription);

			// Test Generic functions
			Assert.AreEqual ("Footprint", g.getItemName);
			Assert.AreEqual ("Size 13 clown shoe", g.getItemDescription);
		}
	}

}
