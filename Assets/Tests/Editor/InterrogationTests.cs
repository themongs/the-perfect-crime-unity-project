﻿using NSubstitute;
using NUnit.Framework;
using System.IO;
using System.Text;
using UnityEngine.UI;

namespace PerfectCrime
{
	public class DummyButton : Button {}
	public class DummyUI : UIWidgets
	{
		public override void setButtonText(int b, string text) {}
	}

	[TestFixture]
	public class InterrogationTests
	{
		game g;

		[SetUp]
		public void setup()
		{
			// Start a new game with a specific random seed.
			g = new game ();
			g.setRandomSeed (131166);

			UIWidgets ui = new DummyUI ();
			g.setUI (ui);

			// Load data.
			g.LoadCharacters (GameTests.getValidCharacterData ());
			g.LoadRooms (GameTests.getValidRoomData());
			g.LoadFurniture (GameTests.getValidFurnitureData ());
			g.LoadWeapons (GameTests.getValidWeaponData ());
			g.LoadClues (GameTests.getValidClueData ());

			// Set up game.
			g.SetupFurniture ();
			g.SetupMurder();
			g.SetupWitnesses();
			g.SetupItems();
		}

		[Test]
		public void testIntroduction ()
		{
			StringBuilder builder = new StringBuilder ();
			builder.Append("Your name is one and you are a Computer Analyst. The following is a description of your character:\n\n");
			builder.Append("Age: 31\n");
			builder.Append ("Height: 173 (tall)\n");
			builder.Append ("Eyes: hazel\n");
			builder.Append ("Hair: blonde\n");
			builder.Append ("Nationality: Russian\n\n");
			builder.Append ("At 11pm you killed four the Plumber in the a.\n");
			builder.Append ("You did it with a gun due to an argument you both had over money.\n");
			builder.Append ("Since there was a party in the dining room, no one heard a sound.\n");
			builder.Append ("You then left the gun by the couch in the a.\n\n");
			builder.Append ("All suspects who were there on the night have been questioned, and you are now the last one.\n");
			builder.Append ("You must evade the detective’s questions, but also use their lack of knowledge to accuse another character of the murder.\n");
			builder.Append ("You can do this by referring to additional evidence to convince the detective someone else did it.\n");
			builder.Append("Good luck... killer.\n");

			Assert.AreEqual (builder.ToString(), g.i.introMurderScenario ());
		}
	}
}
